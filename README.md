# dAmn chat bot for #KoY
## Dependencies
* node.js

All other node dependencies are bundled with the bot however `node-sqlite3` requires compiling and so it is possible you will receive errors when trying to run the bot as the `node-sqlite3` module has not been compiled for your system. If you do receive these errors, delete the `node_modules/sqlite3` directory then run:

```
$ npm install sqlite3
```

Which should compile a version for your system.

## Setup
Before running the bot for the first time, run the following in order to set up OAuth and admin users.

```
$ node setup.js
```

The script creates a `conf.json` file which you must edit appropriately before running the bot. Some of the configuration options are documented here:

* **name** The deviantART account name of the bot. This is case-sensitive.
* **agentName** A string to identify the bot to the dAmn server. Can be anything you want really.
* **oAuthClientID** Leave this alone unless you know what you are doing and would like to authenticate your bot as a different deviantART application to the default.
* **oAuthClientSecret** As above, leave this alone unless you know what you are doing.
* **channels** A list of the channels to join when the bot first starts up.
* **cmdChar** A list of characters that are allowed to prefix commands to the bot.
* **quoteLookback** The maximum number of lines to look back through the logs to find a quote when using the **!quote** command.
* **rotateLogsPreserve** The number of log rows to keep in the database when the logs are moved out and backed up. This should always be greater than or equal to **quoteLookback** otherwise the bot will be unable to find quotes that it should.
* **logDirectory** The directory to store the backed-up logs.
* **quoteChance** The chance that the bot will show the quote of a person joining the room. Set this to 1 if you always want a random quote to be shown (if it exists) or set it to 0 to disable this function entirely.
* **dictionaryKey** The API key to use when doing dictionary searches. You can obtain this by visiting http://www.dictionaryapi.com/. It is set to **false** by default to disable the functionality. Please make sure that, if you get a key, you surround it with quotes (") like the other values in the config file.
* **thesaurusKey** This can be obtained the same way as above.
* **karmaChangeLimitMinutes** The number of minutes users must wait between changing the karma of the same item. Set this to 0 if you would like to allow users to change the karma of the same item whenever they want.
* **pingTimeoutMinutes** The number of minutes the bot will wait to receive a message from the dAmn server before restarting itself. For some reason, occasionally the dAmn server will stop sending messages to a connected bot and the bot will be forever stuck waiting for messages that will never come. To counteract this, the bot will restart itself and thus create a fresh connection after X minutes have passed of not receiving a message from the server. Set this to -1 or omit it from the config file to disable this feature.

## Running the bot

```
$ node persist.js
```

### Command Line Options
Supply any of these arguments on the command line when you invoke persist.js.

* **--pretty-logs** Suppress logging raw server packets and instead format them like a chatroom.
* **--tui** Enables the text user interface to write commands to the bot on the command line directly. This forces **--pretty-logs** on.
* **--debug** Enables some extra debug logging such as warnings on when locks have been held too long.

## Administering the bot
When first running the bot, it should prompt you to enter the username of someone you want to grant administrator privileges to. This user will be able to give special, restricted commands to the bot. There is no limit to the number of adminstrators you may add so if at any point you would like to add or remove a username to this administrator list, run one of the following:

```
$ node admin.js add username
```

```
$ node admin.js remove username
```

## Logging
The bot will log all RECV packets from all channels it is in to `db/log.sq3` and every day these logs will be rotated into the `logs/` directory, compressed and ordered by their year, month, then day.

`logs.js` is a command line tool that can be used to process these logs more conveniently than using raw SQLite3 commands. It can be used as follows:

Converts the logs for the given day (in UTC!) into HTML and places them in the `html` directory.
```
$ node logs.js html --day=YYYY-MM-DD
```

Converts the logs for all days between the start and end day (inclusive) into HTML and places them in the `html` directory.
```
$ node logs.js html --start-day=YYYY-MM-DD --end-day=YYYY-MM-DD
```

Starts a websocket server on the specified port that runs queries over any given range of the logs and returns results as nicely formatted HTML.
```
$ node logs.js server --port=X
```

The server accepts messages in the following JSON format:
```
{
	"start": "YYYY-MM-DD"
	, "end": "YYYY-MM-DD"
	, "query": "SQL"
	, "values": {}
}
```

`query` can be any SQL query string (run on the database `'log'`) and will be run against each day of logs from `start` to `end` inclusively. `values` is a normal javascript object used to bind any parameters in your SQL query string. For example, to check all logs from `2000-01-01` to `2000-01-31` for the phrase `'cat'` you might send the following message to the server:
```
{
	"start": "2000-01-01"
	, "end": "2000-01-31"
	, "query": "SELECT * FROM log WHERE subpacket LIKE $phrase"
	, "values": {
		"$phrase": "%cat%"
	}
}
```

## Contributing
If you'd like to contribute, it's advised you install the pre-commit hook before you commit any code to make sure your code doesn't get automatically rejected for silly errors. The hook simply runs the linter and all the tests before you commit.

```
$ ./install-hooks.sh
```
