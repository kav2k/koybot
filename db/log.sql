CREATE TABLE "log" (
    "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
    "timestamp" INTEGER NOT NULL,
    "channel" TEXT NOT NULL,
    "user" TEXT,
    "subpacket" BLOB NOT NULL
);
