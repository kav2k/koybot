#!/bin/sh
root=$(git rev-parse --show-toplevel)
for hook in `ls "${root}/hooks"`; do
	ln -s "${root}/hooks/${hook}" "${root}/.git/hooks/${hook}"
done
