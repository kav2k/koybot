'use strict';

var fmt = require('util').format;

/**
 * Accepts an array of rows of data and generates two companion objects to be
 * used with the sqlite3 module's Database.run command. The map property
 * contains the mapping from argument name to value. The str property contains
 * the SQL INSERT string that houses all the argument names.
 */

exports.generateInsertSQL = function (data) {
	var alphabet = 'abcdefghijklmnopqrstuvwxyz';
	var valuesStr = [];
	var valuesMap = {};

	var getMapFn = function (i) {
		return function (arg, n) {
			var key = fmt('$%s%d', alphabet[n], i);
			valuesMap[key] = arg;
			return key;
		};
	};

	for (var i = 0; i < data.length; i++) {
		var datum = data[i];
		if (datum.length > alphabet.length) {
			throw new Error('Ran out of characters in the alphabet!');
		}

		var args = datum.map(getMapFn(i));
		valuesStr.push(fmt('(%s)', args.join(', ')));
	}

	return {
		map: valuesMap
		, str: valuesStr.join(', ')
	};
};

/**
 * Hides away the tedious boilerplate of setting up the test databases for
 * integration tests. Takes an optional data object argument with two possible
 * properties 'mainidb' and 'logdb' and inserts data into those databases
 * respectively before calling the callback argument.
 *
 * The data argument has a special first row which specifies the table name
 * and the columns to use in the query.
 *
 * An example data argument would look like:
 * {
 *	logdb: [
 *		{
 *			tableName: 'tbl'
 *			, cols: ['col1', 'col2']
 *		}, ['val1', 'val2']
 *		, ['val3', 'val4']
 *	]
 * }
 */

exports.setupDBs = function (attach, cb, data) {
	var Log = require('./log.js');
	var DatabaseLoader = require('./database_loader.js');
	var fs = require('fs');
	var sqlite3 = require('sqlite3');

	var dbLoader = new DatabaseLoader({
		Log: new Log(false)
		, fs: fs
		, sqlite3: sqlite3
	});

	var addData = function () {
		if (data == null || (data.maindb == null && data.logdb == null)) {
			cb();
			return;
		}

		var dbCount = 0;
		var queriesRun = 0;

		if (data.maindb != null) {
			dbCount++;
		}

		if (data.logdb != null) {
			dbCount++;
		}

		var mapColNames = function (col) {
			return fmt('`%s`', col);
		};

		var handleDBRun = function (err) {
			if (err != null) {
				throw err;
			}

			checkDone();
		};

		var checkDone = function () {
			queriesRun++;

			if (queriesRun === dbCount) {
				cb();
			}
		};

		for (var dbProp in data) {
			if (!data.hasOwnProperty(dbProp)
				|| ['logdb', 'maindb'].indexOf(dbProp) < 0) {

				continue;
			}

			var rows = data[dbProp];
			var header = rows.shift();
			var tableName = header.tableName;
			var cols = header.cols.map(mapColNames).join(', ');
			var sql = exports.generateInsertSQL(rows);

			attach[dbProp].run(
				fmt('INSERT INTO %s (%s) VALUES %s', tableName, cols, sql.str)
				, sql.map
				, handleDBRun);
		}
	};

	var loadLogDB = function () {
		dbLoader.load('tests/db/log.sq3', 'db/log.sql', function (db) {
			if (db == null) {
				throw new Error(
					'Unable to initialise log DB for integration tests.');
			}

			attach.logdb = db;
			addData();
		});
	};

	var loadMainDB = function () {
		dbLoader.load('tests/db/main.sq3', 'db/main.sql', function (db) {
			if (db == null) {
				throw new Error(
					'Unable to initialise main DB for integration tests.');
			}

			attach.maindb = db;
			loadLogDB();
		});
	};

	loadMainDB();
};

exports.cleanupDBs = function (attach, cb) {
	var fs = require('fs');

	var toClose = ['maindb', 'logdb'];
	var toDelete = ['log.sq3', 'main.sq3'];

	var deleteDBs = function () {
		var deleted = 0;
		var checkDeleted = function () {
			deleted++;

			if (deleted === toDelete.length) {
				cb();
			}
		};

		toDelete.forEach(function (dbFile) {
			fs.unlink(fmt('tests/db/%s', dbFile), function (err) {
				if (err != null) {
					throw err;
				}

				checkDeleted();
			});
		});
	};

	var closeDBs = function () {
		var closed = 0;
		var checkClosed = function () {
			closed++;

			if (closed === toClose.length) {
				deleteDBs();
			}
		};

		toClose.forEach(function (dbName) {
			attach[dbName].close(function (err) {
				if (err != null) {
					throw err;
				}

				checkClosed();
			});
		});
	};

	closeDBs();
};

