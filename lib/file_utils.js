'use strict';

var fmt = require('util').format;
var fs = require('fs');
var spawn = require('child_process').spawn;
var zlib = require('zlib');

exports.copyFile = function (src, dest, onComplete) {
	var reader = fs.createReadStream(src);
	var writer = fs.createWriteStream(dest, {mode: 436});

	writer.on('finish', onComplete);
	writer.on('error', function (err) {
		onComplete(err);
	});

	reader.on('error', function (err) {
		fs.unlink(dest);
		onComplete(err);
	});
	reader.pipe(writer);

	return true;
};

exports.deleteDir = function (dir, onComplete) {
	if (onComplete == null) {
		onComplete = function () {};
	}

	var rm = spawn('rm', ['-r', dir]);

	rm.on('exit', function (code, signal) {
		onComplete(code);
	});
};

exports.unzip = function (file, onComplete) {
	var outFile = file.substring(0, file.length - 4);
	var gunzip = zlib.createGunzip();
	var input = fs.createReadStream(file);
	var output = fs.createWriteStream(outFile, {mode: 436});

	var error = function (err) {
		onComplete(1, outFile);
	};

	input.on('error', error);
	gunzip.on('error', error);
	output.on('error', error);

	output.on('finish', function () {
		fs.unlink(file, function (err) {
			onComplete((err == null) ? 0 : 1, outFile);
		});
	});

	input.pipe(gunzip).pipe(output);
};

exports.zip = function (file, onComplete) {
	var outFile = file + '.bz2';
	var gzip = zlib.createGzip({level: zlib.Z_BEST_COMPRESSION});
	var input = fs.createReadStream(file);
	var output = fs.createWriteStream(outFile, {mode: 436});

	var error = function (err) {
		fs.unlink(outFile);
		onComplete(1, outFile);
	};

	input.on('error', error);
	gzip.on('error', error);
	output.on('error', error);

	output.on('finish', function () {
		fs.unlink(file, function (err) {
			onComplete((err == null) ? 0 : 1, outFile);
		});
	});

	input.pipe(gzip).pipe(output);
};
