'use strict';

// Small utility class for treating the main SQLite database as a key-value
// store. It does this by using a special table 'misc' that contains only a key
// and value column.

module.exports = function (Context) {
	var self = this;

	if (Context.maindb == null) {
		Context.Log.error(
			'Unable to initialise key-value store as the '
			+ 'main database has not been opened.');

		return;
	}

	self.get = function (key, cb) {
		if (typeof cb !== 'function') {
			cb = function () {};
		}

		if (key == null || typeof key === 'function') {
			Context.Log.error('Tried to pass invalid key to key-value lookup.');
			cb(null);
			return;
		}

		if (typeof key === 'object') {
			key = JSON.stringify(key);
		}

		var query =
			'SELECT value '
			+ 'FROM misc '
			+ 'WHERE key = $key';

		var values = {$key: key};

		Context.maindb.get(query, values, function (err, row) {
			if (err != null) {
				Context.Log.error(
					'Error performing key-value lookup of key "%s": %s'
					, key
					, err.message);

				cb(null);
				return;
			}

			if (row == null || row.value == null) {
				cb(null);
				return;
			}

			cb(row.value);
		});
	};

	self.put = function (key, value, cb) {
		if (typeof cb !== 'function') {
			cb = function () {};
		}

		if (key == null || typeof key === 'function') {
			Context.Log.error(
				'Tried to pass invalid key to key-value store put.');

			cb(false);
			return;
		}

		if (typeof key === 'object') {
			key = JSON.stringify(key);
		}

		if (value === undefined || typeof value === 'function') {
			Context.Log.error(
				'Tried to pass invalid value to '
				+ 'key-value store put for key "%s".'
				, key);

			cb(false);
			return;
		}

		if (typeof value === 'object') {
			value = JSON.stringify(value);
		}

		var query =
			'REPLACE INTO misc '
			+	'(id, key, value) '
			+ 'VALUES ('
			+	'(SELECT id FROM misc WHERE key = $key)'
			+	', $key'
			+	', $value'
			+ ')';

		var values = {$key: key, $value: value};

		Context.maindb.run(query, values, function (err) {
			if (err != null) {
				Context.Log.error(
					'Error updating key-value store for pair ("%s", "%s"): %s'
					, key
					, value
					, err.message);

				cb(false);
				return;
			}

			cb(true);
		});
	};
};
