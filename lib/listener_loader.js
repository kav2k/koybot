'use strict';

module.exports = function (Context, EventProcessor, dAmn) {
	var self = this;

	var clearModuleCache = function () {
		// For now, we can probably remove the entire module cache. Most
		// modules are already cached in Context and we should be able to endure
		// a couple of disk hits for those that aren't on the rare occasion
		// that listeners are reloaded.

		for (var module in require.cache) {
			delete require.cache[module];
		}
	};

	self.load = function (dir) {
		if (Context.maindb == null || Context.logdb == null) {
			Context.Log.error(
				'Databases unavailable, loading listeners aborted.');
			return;
		}

		clearModuleCache();

		var files = [];
		try {
			files = Context.fs.readdirSync(dir);
		} catch (e) {
			Context.Log.error('Unable to load listeners: %s', e.message);
		}

		for (var i = 0; i < files.length; i++) {
			var file = files[i];
			if (file === 'disabled') {
				// Ignore the directory we place disabled listeners in.
				continue;
			}

			var requirePath = Context.fmt('./%s/%s', dir, file);
			var Listener;

			try {
				Listener = require(requirePath);
			} catch (e) {
				requirePath = Context.fmt('../%s/%s', dir, file);
				try {
					Listener = require(requirePath);
				} catch (e) {
					Context.Log.error('Could not initialise listener %s', file);
					continue;
				}
			}

			new Listener(Context, EventProcessor, dAmn);
		}
	};
};
