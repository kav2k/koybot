'use strict';

module.exports = function (Context) {
	var self = this;
	var locks = 0;
	var DEFAULT_WARN_THRESHOLD = 60 * 1000;

	self.create = function (warnThreshold) {
		if (warnThreshold == null) {
			warnThreshold = DEFAULT_WARN_THRESHOLD;
		}

		locks += 1;
		return new Mutex(locks, warnThreshold);
	};

	var Mutex = function (lockID, warnThreshold) {
		var self = this;
		var lock = false;
		var queue = [];
		var timeout = null;

		var warn = function () {
			Context.Log.log(
				'WARN: Lock #%s has been held for over %ss...'
				, lockID
				, warnThreshold / 1000);
		};

		self.acquire = function (fn) {
			if (lock) {
				queue.push(fn);
				return false;
			} else {
				if (Context.cmdLine.debug) {
					Context.Log.log('Acquiring lock #%s...', lockID);
					timeout = setTimeout(warn, warnThreshold);
				}

				lock = true;
				fn();
				return true;
			}
		};

		self.release = function () {
			if (Context.cmdLine.debug) {
				Context.Log.log('Releasing lock #%s...', lockID);
				clearTimeout(timeout);
			}

			lock = false;
			if (queue.length > 0) {
				var fn = queue.shift();
				self.acquire(fn);
			}
		};
	};
};
