'use strict';

var util = require('util');

module.exports = function (tui) {
	var self = this;
	var Stream = Object.freeze({
		LOG: 1
		, ERROR: 2
	});

	var write = function (stream, args) {
		var out;
		switch (stream) {
			case Stream.LOG:
				out = process.stdout;
				break;

			case Stream.ERROR:
				out = process.stderr;
				break;

			default:
				console.error('Trying to log to unknown stream.');
				return;
		}

		var str = util.format.apply(self, args);

		if (tui) {
			process.send(str);
		} else {
			out.write(str + '\n');
		}
	};

	self.log = function (msg) {
		if (msg == null) {
			return;
		}

		var args = Array.prototype.slice.call(arguments, 0);
		write(Stream.LOG, args);
	};

	self.error = function (msg) {
		if (msg == null) {
			return;
		}

		var args = Array.prototype.slice.call(arguments, 0);
		write(Stream.ERROR, args);
	};
};
