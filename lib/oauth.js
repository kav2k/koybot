'use strict';

module.exports = function (Context) {
	var self = this;
	var expires = -1;
	self.store = null;
	self.dAmnToken = null;

	var oAuthStore = './oauth.json';
	if (Context.config && Context.config.oAuthStore != null) {
		oAuthStore = Context.config.oAuthStore;
	}

	try {
		self.store = Context.fs.readFileSync(oAuthStore);
	} catch (e) {}

	if (self.store != null && self.store.length > 0) {
		try {
			self.store = JSON.parse(self.store);
		} catch (e) {
			Context.Log.error(
				'Unable to read persistent oAuth data: %s'
				, e.message);
		}
	} else {
		self.store = null;
	}

	var getDAmnToken = function (accessToken, fn) {
		var data = '';
		var req = Context.spawn('curl', [
			'--silent'
			, 'https://www.deviantart.com/api/oauth2/user/damntoken'
			, '-d', 'access_token=' + accessToken
		]);

		req.stdout.on('data', function (d) {
			data += d;
		});

		req.on('close', function (code) {
			var damntoken = null;
			try {
				damntoken = JSON.parse(data);
			} catch (e) {
				Context.Log.error(
					'Invalid response when retrieving dAmn token: %s'
					, data);
				expires = -1;
				return;
			}

			if (damntoken.damntoken == null) {
				Context.Log.error(
					'Invalid response when retrieving dAmn token: %s'
					, data);
				expires = -1;
			}

			self.dAmnToken = damntoken.damntoken;
			fn(self.dAmnToken);
		});

		req.stderr.on('data', function (data) {
			Context.Log.error(data.toString());
		});
	};

	self.getAccessToken = function (clientID, clientSecret, fn) {
		if (fn == null || typeof fn !== 'function') {
			Context.Log.error('oauth.refresh requires a callback argument.');
			return;
		}

		if (self.store == null) {
			Context.Log.error(
				'Cannot refresh oAuth token, no refresh token available.');

			return;
		}

		if (new Date().valueOf() < expires) {
			// Token hasn't expired, no need to refresh.
			fn(self.dAmnToken);
			return;
		}

		var data = '';
		var req = Context.spawn('curl', [
			'--silent'
			, 'https://www.deviantart.com/oauth2/token'
			, '-d', 'grant_type=refresh_token'
			, '-d', 'client_id=' + clientID
			, '-d', 'client_secret=' + clientSecret
			, '-d', 'refresh_token=' + self.store.refresh_token
		]);
		
		req.stdout.on('data', function (d) {
			data += d;
		});

		req.on('close', function (code) {
			try {
				self.store = JSON.parse(data);
			} catch (e) {
				Context.Log.error(
					'FATAL: Unable to refresh OAuth token: %s'
					, e.message);
				return;
			}

			if (self.store.expires_in == null
				|| self.store.access_token == null) {

				Context.Log.error(
					'OAuth token response corrupt: %s'
					, data);
				return;
			}

			Context.fs.writeFile(
				oAuthStore
				, data
				, {mode: 436}
				, function (err) {
					if (err != null) {
						Context.Log.error(
							'Error persisting OAuth data, '
							+ 'bot will be unable to '
							+ 'authenticate in future: %s'
							, err.message);
					}

					expires =
						new Date().valueOf() + self.store.expires_in * 1000;

					getDAmnToken(self.store.access_token, fn);
				}
			);
		});

		req.stderr.on('data', function (data) {
			Context.Log.error(data.toString());
		});
	};
};
