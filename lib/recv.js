'use strict';

module.exports = function (Context, EventProcessor) {
	var self = this;
	self.Actions = Object.freeze({
		MSG: 'msg'
		, ACTION: 'action'
		, JOIN: 'join'
		, PART: 'part'
		, KICKED: 'kicked'
	});

	var handleMsg = function (recv) {
		if (Context.cmdLine.prettyLogs) {
			var now = new Date();
			Context.Log.log(
				'%s \u001B[1m<%s>\u001B[0m %s'
				, Context.DateUtils.getPrettyTime(now)
				, recv.from
				, recv.content.trim());
		}

		EventProcessor.fire('recvMsg', recv);

		if (recv.content != null
			&& Context.config.cmdChar != null
			&& recv.content.length > 1
			&& Context.config.cmdChar.indexOf(recv.content.charAt(0)) > -1
			&& Context.config.name.toLowerCase() !== recv.from.toLowerCase()) {

			var contents = recv.content.toLowerCase();
			var spacePos = contents.indexOf(' ');
			if (spacePos < 0) {
				spacePos = contents.length;
			}

			var eventName = 'cmd_' + contents.substring(1, spacePos);
			EventProcessor.fire(eventName, recv);
		}
	};

	var handleJoin = function (recv) {
		if (Context.cmdLine.prettyLogs) {
			var now = new Date();
			Context.Log.log(
				'%s \u001B[1m** %s\u001B[0m has joined.'
				, Context.DateUtils.getPrettyTime(now)
				, recv.from);
		}

		EventProcessor.fire('recvJoin', recv);
	};

	var handleAction = function (recv) {
		if (Context.cmdLine.prettyLogs) {
			var now = new Date();
			Context.Log.log(
				'%s \u001B[1m* %s\u001B[0m %s'
				, Context.DateUtils.getPrettyTime(now)
				, recv.from
				, recv.content.trim());
		}
	};

	var handlePart = function (recv) {
		if (Context.cmdLine.prettyLogs) {
			var now = new Date();
			Context.Log.log(
				'%s \u001B[1m** %s\u001B[0m has left.'
				, Context.DateUtils.getPrettyTime(now)
				, recv.from);
		}
	};

	var handleKick = function (kick) {
		if (Context.cmdLine.prettyLogs) {
			var now = new Date();
			Context.Log.log(
				'%s \u001B[1m** %s was kicked by %s.\u001B[0m %s'
				, Context.DateUtils.getPrettyTime(now)
				, kick.kicked
				, kick.by
				, (kick.reason != null) ? kick.reason : '');
		}
	};

	self.handle = function (packet) {
		if (packet.body == null) {
			Context.Log.error('Received corrupt RECV packet.');
			return;
		}

		var channel = Context.Packet.getChannel(packet);
		var subPacket = Context.Packet.parse(packet.body);

		if (channel == null || subPacket == null) {
			Context.Log.error('Received corrupt RECV packet.');
			return;
		}

		if (subPacket.args == null) {
			subPacket.args = {};
		}

		EventProcessor.fire('recv', channel, subPacket, packet.body);

		var recv = {
			channel: channel
			, action: subPacket.cmd
			, from: subPacket.args.from || subPacket.param
			, content: subPacket.body
		};

		switch (recv.action) {
			case self.Actions.MSG:
				handleMsg(recv);
				break;

			case self.Actions.JOIN:
				handleJoin(recv);
				break;

			case self.Actions.ACTION:
				handleAction(recv);
				break;

			case self.Actions.PART:
				handlePart(recv);
				break;

			case self.Actions.KICKED:
				var kick = {
					channel: channel
					, kicked: subPacket.param
					, by: subPacket.args.by
					, reason: subPacket.body
				};

				handleKick(kick);
				break;

			default:
				if (!Context.cmdLine.prettyLogs) {
					Context.Log.log('Unhandled RECV action: %s', recv.action);
				}
		}
	};
};
