'use strict';

exports.create = function () {
	var RefCount = function () {
		var self = this;
		var count = 0;

		self.allocate = function () {
			count++;
		};

		self.free = function () {
			count--;

			if (count < 0) {
				count = 0;
				throw new Error('RefCount was reduced to below 0.');
			}
		};

		self.inUse = function () {
			return count > 0;
		};
	};

	return new RefCount();
};
