'use strict';

var fmt = require('util').format;
var moment = require('moment');

exports.getMockMoment = function (baseTime) {
	return function () {
		return moment(baseTime).utc();
	};
};

exports.injectError = function (msg) {
	if (msg == null) {
		msg = 'bad';
	}

	return function (query, values, fn) {
		if (fn == null) {
			fn = values;
		}

		fn({message: msg});
	};
};

exports.interceptLogging = function (Context, dest) {
	if (!Context.Log) {
		Context.Log = {};
	}

	Context.Log.error = function () {
		var args = Array.prototype.slice.call(arguments, 0);
		dest.error = fmt.apply(dest, args);
	};

	Context.Log.log = function () {
		var args = Array.prototype.slice.call(arguments, 0);
		dest.log = fmt.apply(dest, args);
	};
};

exports.getMockLocks = function () {
	return {
		create: function () {
			return {
				release: function () {}
				, acquire: function (fn) {
					fn();
					return true;
				}
			};
		}
	};
};

exports.getMockPacket = function (ret) {
	if (ret === undefined) {
		ret = false;
	}

	return {
		hasArg: function (packet) {
			return ret;
		}
	};
};

exports.clearRequireCache = function () {
	for (var module in require.cache) {
		delete require.cache[module];
	}
};

exports.getMockContext = function (attach) {
	exports.clearRequireCache();

	var Context = {};
	var Locks = require('./locks.js');
	var Packet = require('./packet.js');
	var Tablumps = require('./tablumps.js');

	Context.MathsUtils = require('./maths_utils.js');
	Context.DateUtils = require('./date_utils.js');
	Context.Locks = exports.getMockLocks();
	Context.moment = moment;
	Context.fmt = fmt;
	Context.HelpText = {};
	Context.Admin = {};
	Context.maindb = {};

	exports.interceptLogging(Context, attach);
	Context.Packet = new Packet(Context);
	Context.Tablumps = new Tablumps(Context);

	return Context;
};

exports.getMockEventProcessor = function (trigger, recv) {
	if (recv === undefined) {
		recv = trigger;
		trigger = false;
	}

	return {
		register: function (evt, fn) {
			if (!trigger) {
				fn(recv);
			} else if (evt === trigger) {
				fn(recv);
			}
		}
	};
};

exports.getMockIsAdmin = function (err, isAdmin) {
	return function (db, admin, fn) {
		fn(err, isAdmin);
	};
};
