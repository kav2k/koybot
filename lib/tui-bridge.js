'use strict';

module.exports = function (Context) {
	var self = this;
	var dAmn = null;
	var channel = null;

	self.setup = function (o) {
		dAmn = o;
	};

	self.joinChannel = function (chan) {
		if (dAmn == null) {
			Context.Log.error(
				'TUI bridge was set up out of order. '
				+ 'Tried to join channel before receiving dAmn object.');

			return;
		}

		channel = chan;
	};

	var talk = function (msg) {
		dAmn.sendMsg(channel, msg);
	};

	var changeChannel = function (newChan) {
		if (newChan == null) {
			return;
		}

		channel = newChan;
	};

	var userInput = function (msg) {
		if (dAmn == null || channel == null) {
			Context.Log.log('Chat not yet connected, input discarded.');
			return;
		}

		var splitOnSpace = msg.split(' ');
		if (splitOnSpace.length < 1) {
			return;
		}

		switch (splitOnSpace[0]) {
			case '/chan':
				changeChannel(splitOnSpace[1]);
				break;

			default:
				talk(msg);
		}
	};

	process.on('message', userInput);
};
