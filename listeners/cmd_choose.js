'use strict';

module.exports = function (Context, EventProcessor, dAmn) {
	Context.HelpText.choose =
		'Usage:\n'
		+ '	!choose <b>A</b> or <b>B</b> or ... - Selects randomly between the '
		+ 'choices separated by the word \'or\'.';

	EventProcessor.register('cmd_choose', function (recv) {
		var choiceString = Context.Packet.hasArg(recv);
		if (!choiceString) {
			return;
		}

		var choices = choiceString.split(' or ');
		if (choices.length < 2) {
			return;
		}

		var choice =
			choices[Context.MathsUtils.randomInt(0, choices.length - 1)];

		dAmn.sendMsg(recv.channel, Context.fmt('%s: %s', recv.from, choice));
	});
};
