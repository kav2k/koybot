'use strict';

module.exports = function (Context, EventProcessor, dAmn) {
	var validForms = ['noun', 'verb', 'adjective'];

	var maxDefinitions = 5;
	if (Context.config != null && Context.config.maxDefinitions != null) {
		maxDefinitions = Context.config.maxDefinitions;
	}

	Context.HelpText.define = Context.fmt(
		'Usage:\n'
		+ '	!define <b>word</b> - Try to find the given word in the dictionary '
		+ 'and print the first %s definitions.\n'
		+ '	!define <b>word</b> <b>noun/adjective/verb</b> - Try to find the '
		+ 'given word of the form specified and print its definitions.\n'
		+ '	!define <b>word</b> <b>noun/adjective/verb</b> #<b>number<b/> - '
		+ 'For words that have multiple definitions of the same form, print '
		+ 'the one specified by <b>number</b>.\n'
		+ '	!define <b>word</b> <b>full</b> - You may add <b>full</b> to the '
		+ 'end of any of the above commands to print all definitions of that '
		+ 'form (but you may get banned for spamming).'
		, maxDefinitions);

	var disabled = function (recv) {
		dAmn.sendMsg(
			recv.channel
			, Context.fmt(
				'%s: That command is currently disabled.'
				, recv.from));
	};

	var sendSuggestion = function (recv, suggestion) {
		dAmn.sendMsg(
			recv.channel
			, Context.fmt(
				'%s: Did you mean <b><i>%s</i></b>?'
				, recv.from
				, suggestion));
	};

	var notFound = function (recv) {
		dAmn.sendMsg(
			recv.channel
			, Context.fmt(
				'%s: No definitions were found which '
				+ 'matched your search parameters.'
				, recv.from));
	};

	var constructDefinition = function (recv, full, form, formCounts, entries) {
		var entry = form.parent;
		var pronunciation = entry.childNamed('pr');
		var i = 0;

		if (pronunciation == null) {
			for (i = 0; i < entries.length; i++) {
				var pr = entries[i].childNamed('pr');
				if (pr != null) {
					pronunciation = pr;
					break;
				}
			}
		}

		if (pronunciation == null) {
			pronunciation = '';
		} else {
			pronunciation =
				Context.fmt('/%s/', Context.ent.encode(pronunciation.val));
		}

		var def = entry.childNamed('def');
		var definitions = [];
		if (def != null) {
			definitions = def.childrenNamed('dt');
		}

		if (definitions.length < 1) {
			notFound(recv);
			return;
		}

		var definitionStrings = [];
		for (i = 0; i < definitions.length; i++) {
			if (full === false && i >= maxDefinitions) {
				break;
			}

			var definition = definitions[i];
			var text = definition.val;

			for (var n = 0; n < definition.children.length; n++) {
				if (definition.children[n].name === 'vi') {
					continue;
				}

				text += definition.children[n].val;
			}

			definitionStrings.push(text);
		}

		var definitionBlock = '';
		definitionStrings.forEach(function (def, index) {
			definitionBlock += Context.fmt(
				'&nbsp;&nbsp;&nbsp;&nbsp; %s. %s<br />'
				, index + 1
				, def.replace(/^:(\w)/g, '$1').replace(/ :(\w)/g, '; $1'));
		});

		var formsFound = [];
		for (var fl in formCounts) {
			var count = formCounts[fl];
			formsFound.push(count + ' ' + fl + ((count > 1) ? 's' : ''));
		}

		if (formsFound.length < 1) {
			Context.log.error(
				'Inconsistent state when defining a word. '
				+ 'A definition was found but no forms.');

			return;
		}

		var formList = formsFound[0];
		if (formsFound.length > 1) {
			var last = formsFound.pop();
			formList = formsFound.join(', ') + ' and ' + last;
		}

		formList += ' found.';

		var msg = Context.fmt(
			'%s<br />%s, <i>%s</i> %s<br />%s'
			, formList
			, entry.childNamed('ew').val
			, form.val
			, pronunciation
			, definitionBlock);

		dAmn.sendMsg(recv.channel, msg);
	};

	var handleResponse = function (recv, word, form, formNumber, full, xml) {
		var document = new Context.xml.XmlDocument(xml);
		if (Object.keys(document).length < 1) {
			Context.Log.error(
				'Invalid XML received when looking up "%s".'
				, word);

			notFound(recv);
			return;
		}

		var suggestion = document.childNamed('suggestion');
		if (suggestion != null && suggestion.val !== '') {
			sendSuggestion(recv, suggestion.val);
			return;
		}

		var entries = [];
		document.children.forEach(function (entry) {
			var ew = entry.childNamed('ew');
			if (ew == null || ew.val.toLowerCase() !== word) {
				return;
			}

			entries.push(entry);
		});

		var forms = [];
		entries.forEach(function (entry) {
			var fl = entry.childNamed('fl');
			if (fl != null) {
				fl.parent = entry;
				forms.push(fl);
			}
		});

		if (forms.length < 1) {
			Context.Log.error(
				'Valid XML was received but seemed to contain no '
				+ 'dictionary data when looking up "%s".'
				, word);

			notFound(recv);
			return;
		}

		var targetElement = null;
		var formCount = 0;
		for (var i = 0; i < forms.length; i++) {
			var element = forms[i];

			if (validForms.indexOf(element.val) < 0) {
				continue;
			}

			if (form === null) {
				targetElement = element;
				break;
			}

			if (element.val.toLowerCase() === form) {
				formCount++;
				if (formNumber === null || formNumber === formCount) {
					targetElement = element;
					break;
				}
			}
		}

		if (targetElement === null) {
			notFound(recv);
			return;
		}

		var formCounts = {};
		forms.forEach(function (fl) {
			var key = fl.val;

			if (validForms.indexOf(key) < 0) {
				return;
			}

			if (formCounts[key] === undefined) {
				formCounts[key] = 0;
			}

			formCounts[key]++;
		});

		constructDefinition(recv, full, targetElement, formCounts, entries);
	};

	var lookup = function (recv, word, form, formNumber, full) {
		var url = Context.fmt(
			'http://www.dictionaryapi.com/api/v1/references/collegiate/xml/%s'
			+ '?key=%s'
			, word.toLowerCase()
			, Context.config.dictionaryKey);

		var req = Context.http.get(url, function (res) {
			var content = '';
			res.on('data', function (data) {
				content += data;
			});

			res.on('end', function () {
				handleResponse(recv, word, form, formNumber, full, content);
			});
		});

		req.on('error', function (err) {
			Context.Log.error(
				'There was an error looking up the definition of "%s": %s'
				, word
				, err.message
			);
		});
	};

	EventProcessor.register('cmd_define', function (recv) {
		if (Context.config == null || !Context.config.dictionaryKey) {
			disabled(recv);
			return;
		}

		var commands = recv.content.split(' ');
		if (commands.length < 2) {
			return;
		}

		var form = null;
		var formNumber = null;
		var word = commands[1];

		if (commands.length > 2) {
			if (validForms.indexOf(commands[2]) > -1) {
				form = commands[2].toLowerCase();
			} else {
				lookup(recv, word, form, formNumber, commands[2] === 'full');
				return;
			}
		}

		if (commands.length > 3) {
			if (commands[3][0] === '#') {
				formNumber = parseInt(commands[3].substring(1));
				if (isNaN(formNumber)) {
					lookup(recv, word, form, null, false);
				}
			} else {
				lookup(recv, word, form, formNumber, commands[3] === 'full');
				return;
			}
		}

		lookup(recv, word, form, formNumber, commands[4] === 'full');
	});
};
