'use strict';

module.exports = function (Context, EventProcessor, dAmn) {
	Context.HelpText.quote =
		"Usage:\n"
		+ "	!quote - Quotes the most recent line.\n"
		+ "	!quote <b>search</b> - Quotes the most recent line that contains "
		+ "the whole search term.\n"
		+ "	!quote &lt;<b>username</b>&gt; <b>search</b> - Quotes the most "
		+ "recent line of a specific user that contains the whole search term.";

	var self = this;
	var DEFAULT_LOG_CHECK_LIMIT = 100;

	var logCheckLimit = DEFAULT_LOG_CHECK_LIMIT;
	if (Context.config != null && Context.config.quoteLookback != null) {
		logCheckLimit = Context.config.quoteLookback;
	}

	var saveRecentQuote = function (quoteID) {
		Context.memory.recentQuoteID = quoteID;
		Context.KeyValueStore.put('RecentQuoteID', quoteID);
	};

	var quote = function (recv, row, subPacket) {
		var query =
			'INSERT INTO quotes '
			+ '(time_quoted, time_said, channel'
			+ ', quoter, quoted, quote, msg_type) '
			+ 'VALUES ($time_quoted, $time_said, $channel'
			+ ', $quoter, $quoted, $quote, $msg_type)';

		var values = {
			$time_quoted: new Date().getTime()
			, $time_said: row.timestamp
			, $channel: recv.channel
			, $quoter: recv.from
			, $quoted: subPacket.args.from || subPacket.param
			, $quote: subPacket.body
			, $msg_type: subPacket.cmd
		};

		Context.maindb.run(query, values, function (err) {
			if (err != null) {
				Context.Log.error(
					'Error attempting to store quote: %s', err.message);
				return;
			}

			if (this.lastID == null) {
				Context.Log.error(
					'Unable to retrieve last inserted '
					+ 'row ID when storing quote.');
				return;
			}

			saveRecentQuote(this.lastID);
			var msg = Context.fmt('Added quote #%d.', this.lastID);
			dAmn.sendMsg(recv.channel, msg);
		});
	};

	var searchRowsForQuote = function (recv, searchString, rows, lastLine) {
		for (var i = 0; i < rows.length; i++) {
			var row = rows[i];
			if (row.subpacket == null) {
				continue;
			}

			var subPacket = Context.Packet.parse(row.subpacket);
			if (subPacket == null
				|| subPacket.body == null
				|| subPacket.body.length < 1) {

				continue;
			}

			if (Context.config.cmdChar.indexOf(subPacket.body[0]) > -1) {
				// Do not quote commands.
				continue;
			}

			if (lastLine) {
				quote(recv, row, subPacket);
				break;
			}

			var haystack = subPacket.body.toLowerCase();
			var needle = searchString.toLowerCase();

			if (haystack.indexOf(needle) > -1) {
				quote(recv, row, subPacket);
				break;
			}
		}
	};

	var getLastLoggedRows = function (recv, searchString, user, lastLine) {
		var userCondition = 'AND user <> $user ';
		if (user != null) {
			userCondition = 'AND user = $user ';
		}

		var query =
			'SELECT user, subpacket, timestamp '
			+ 'FROM log '
			+ 'WHERE channel = $channel '
				+ userCondition
			+ 'ORDER BY timestamp DESC '
			+ 'LIMIT $limit '
			+ 'COLLATE NOCASE';

		var values = {
			$channel: recv.channel
			, $limit: logCheckLimit
			, $user: (user == null) ? Context.config.name : user
		};

		Context.logdb.all(query, values, function (err, rows) {
			if (err != null) {
				Context.Log.error(
					'Error retrieving recent log rows '
					+ 'to search for quote: %s'
					, err.message);
				return;
			}

			searchRowsForQuote(recv, searchString, rows, lastLine);
		});
	};

	EventProcessor.register('cmd_quote', function (recv) {
		var lastLine = false;
		var user = null;

		var spacePos = recv.content.indexOf(' ');
		var searchString = recv.content.substring(spacePos + 1);

		if (spacePos < 0 || searchString.length < 1) {
			lastLine = true;
		} else {
			var userSearch = searchString.match(/^&lt;([^&]+)&gt; ?(.*)$/);
			if (userSearch != null && userSearch[1] != null) {
				user = userSearch[1];
				if (userSearch[2] != null) {
					searchString = userSearch[2];
				} else {
					lastLine = true;
				}
			}
		}

		getLastLoggedRows(recv, searchString, user, lastLine);
	});
};
