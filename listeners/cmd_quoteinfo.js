'use strict';

module.exports = function (Context, EventProcessor, dAmn) {
	Context.HelpText.quoteinfo =
		"Usage:\n"
		+ "	!quoteinfo - Retrieves information about the last quote to be "
		+ "quoted or the most recent greeting quote.\n"
		+ "	!quoteinfo #<b>ID</b> - Retrieves the quote with a specific ID "
		+ "number";

	var sendResult = function (recv, row) {
		dAmn.sendMsg(
			recv.channel
			, Context.fmt(
				'Quote #%d, quoted by %s on %s: &lt;%s&gt; %s'
				, row.id
				, row.quoter
				, Context.DateUtils.getPrettyDate(new Date(row.time_quoted))
				, row.quoted
				, row.quote));
	};

	var getRecentQuote = function (recv) {
		if (Context.memory.recentQuoteID == null) {
			return;
		}

		var quoteID = Context.memory.recentQuoteID;
		var query =
			'SELECT id, quoter, time_quoted, quoted, quote '
			+ 'FROM quotes '
			+ 'WHERE id = $id';

		var values = {$id: quoteID};

		Context.maindb.get(query, values, function (err, row) {
			if (err != null) {
				Context.Log.error(
					'Error retrieving quote info for quote #%d: %s'
					, quoteID
					, err.message);
			}

			if (row == null) {
				return;
			}

			sendResult(recv, row);
		});
	};

	EventProcessor.register('cmd_quoteinfo', function (recv) {
		var searchString = Context.Packet.hasArg(recv);
		if (!searchString) {
			getRecentQuote(recv);
			return;
		}

		var idMatch = searchString.match(/^#(\d+)/);
		if (idMatch != null && idMatch[1] != null) {
			EventProcessor.fire('cmd_findquote', recv);
			return;
		}
	});

	EventProcessor.register('init', function () {
		Context.KeyValueStore.get('RecentQuoteID', function (value) {
			var valueInt = parseInt(value);
			if (!isNaN(valueInt)) {
				Context.memory.recentQuoteID = valueInt;
			}
		});
	});
};
