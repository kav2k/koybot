'use strict';

module.exports = function (Context, EventProcessor, dAmn) {
	Context.HelpText.randomquote =
		"Usage:\n"
		+ "	!randomquote - Retrieves a random quote from the database.\n"
		+ "	!randomquote <b>username</b> - Retrieves a random quote attributed "
		+ "to a specific user from the database.";

	var sendQuote = function (recv, quote) {
		dAmn.sendMsg(
			recv.channel
			, Context.fmt(
				'#%d &lt;%s&gt; %s'
				, quote.id
				, quote.quoted
				, Context.Tablumps.convert(quote.quote)));
	};

	var randomQuote = function (recv, user, numQuotes) {
		var offset = Context.MathsUtils.randomInt(0, numQuotes - 1);

		var userFilter = '';
		var values = {$offset: offset};

		if (user != null) {
			userFilter = 'WHERE quoted = $quoted ';
			values.$quoted = user;
		}

		var query =
			'SELECT id, quoted, quote '
			+ 'FROM quotes '
			+ userFilter
			+ 'LIMIT 1 '
			+ 'OFFSET $offset';

		Context.maindb.get(query, values, function (err, row) {
			if (err != null) {
				Context.Log.error(
					'Unable to retrieve quote at offset %d: %s'
					, offset
					, err.message);
				return;
			}

			if (row == null
				|| row.quote == null
				|| row.id == null
				|| row.quoted == null) {

				return;
			}

			sendQuote(recv, row);
		});
	};

	var countQuotes = function (recv, user) {
		var userFilter = '';
		var values = {};

		if (user != null) {
			userFilter = ' WHERE quoted = $quoted';
			values.$quoted = user;
		}

		var query =
			'SELECT count(*) '
			+ 'FROM quotes'
			+ userFilter;

		Context.maindb.get(query, values, function (err, row) {
			if (err != null) {
				Context.Log.error(
					'Unable to determine number of quotes: %s'
					, err.message);
				return;
			}

			if (row == null || row['count(*)'] == null || row['count(*)'] < 1) {
				return;
			}

			randomQuote(recv, user, row['count(*)']);
		});
	};

	EventProcessor.register('cmd_randomquote', function (recv) {
		var user = null;
		var userString = Context.Packet.hasArg(recv);

		if (userString) {
			var userSearch = userString.match(/^(&lt;)?([^ &]+)(&gt;)?.*$/);
			if (userSearch != null && userSearch[2] != null) {
				user = userSearch[2];
			}
		}

		countQuotes(recv, user);
	});
};
