'use strict';

module.exports = function (Context, EventProcessor, dAmn) {
	Context.HelpText.reload =
		"Usage:\n"
		+ "	!reload - Reloads all event listeners.";

	var accessDenied = function (recv) {
		dAmn.sendMsg(recv.channel, 'Access denied.');
	};

	var doReload = function (recv) {
		dAmn.sendMsg(recv.channel, 'Reloading listeners.');

		EventProcessor.clear();

		var ListenerLoader = require('../lib/listener_loader.js');
		ListenerLoader = new ListenerLoader(Context, EventProcessor, dAmn);
		ListenerLoader.load('listeners');
	};

	var checkAdmin = function (recv, err, isAdmin) {
		if (err != null) {
			Context.Log.error(
				'Unable to check if "%s" has admin privileges '
				+ 'to run reload command: %s'
				, recv.from
				, err.message);

			return;
		}

		if (isAdmin) {
			doReload(recv);
		} else {
			accessDenied(recv);
		}
	};

	EventProcessor.register('cmd_reload', function (recv) {
		Context.Admin.isAdmin(
			Context.maindb
			, recv.from
			, checkAdmin.curry(recv));
	});
};
