'use strict';

module.exports = function (Context, EventProcessor, dAmn) {
	Context.HelpText.reply = Context.fmt(
		"Usage:\n"
		+ "	!reply <b>text</b> - Ask %s to reply to you directly with the "
		+ "specified text."
		, Context.config.name);

	EventProcessor.register('cmd_reply', function (recv) {
		var text = Context.Packet.hasArg(recv);
		if (!text) {
			return;
		}

		dAmn.sendMsg(recv.channel, Context.fmt('%s: %s', recv.from, text));
	});
};
