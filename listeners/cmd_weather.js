'use strict';

// Tell JSHint that we're using the non-standard escape function.
/* global escape: false */
module.exports = function (Context, EventProcessor, dAmn) {
	Context.HelpText.weather =
		'Usage:\n'
		+ '	!weather <b>city</b> - Retrieves the current weather information '
		+ 'for the given city. If multiple cities with the same name are '
		+ 'found, returns information from the first one.\n'
		+ '	!weather <b>city</b>, <b>country code</b> - Retrieves the current '
		+ 'weather information for the given city in the given country.';

	var handleResponse = function (recv, dataStr) {
		var response = null;
		try {
			response = JSON.parse(dataStr);
		} catch (e) {
			Context.Log.error(
				'Unable to parse response "%s" when checking weather: %s'
				, dataStr
				, e.message);

			dAmn.sendMsg(
				recv.channel
				, 'Bad response from weather server, please try again.');

			return;
		}

		if (response.name == null
			|| response.sys == null
			|| response.sys.country == null
			|| response.weather == null
			|| response.weather.length < 1
			|| response.weather[0].description == null
			|| response.main == null
			|| response.main.temp == null
			|| response.main.humidity == null) {

			Context.Log.error(
				'Corrupt response when retrieving weather information: %j'
				, response);

			if (response.message != null) {
				dAmn.sendMsg(recv.channel, response.message);
			}

			return;
		}

		dAmn.sendMsg(
			recv.channel
			, Context.fmt(
				'%s: %s, %s, %s&#176;c, %s%%, <i>%s</i>'
				, recv.from
				, response.name
				, response.sys.country
				, response.main.temp
				, response.main.humidity
				, response.weather[0].description));
	};

	var getWeather = function (recv, find) {
		var url = Context.fmt(
			'http://api.openweathermap.org/data/2.5/weather?q=%s&units=metric'
			, escape(find));

		var req = Context.http.get(url, function (res) {
			var contents = '';

			res.on('data', function (data) {
				contents += data.toString();
			});

			res.on('end', function () {
				handleResponse(recv, contents);
			});
		});

		req.on('error', function (err) {
			Context.Log.error(
				'Error retrieving weather information for "%s": %s'
				, find
				, err.message);
		});
	};

	EventProcessor.register('cmd_weather', function (recv) {
		var searchString = Context.Packet.hasArg(recv);
		if (!searchString) {
			return;
		}

		getWeather(recv, searchString);
	});
};
