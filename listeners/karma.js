'use strict';

module.exports = function (Context, EventProcessor, dAmn) {
	var transactionLock = Context.Locks.create();
	var karmaChangeLimit = 1000 * 60 * 15; // 15 minutes.
	if (Context.config != null
		&& Context.config.karmaChangeLimitMinutes != null) {

		karmaChangeLimit = Context.config.karmaChangeLimitMinutes * 60 * 1000;
	}

	var addReason = function (item, delta, reason, user) {
		var query =
			'INSERT INTO karma_reasons '
			+ '(item_id, delta, reason, user, timestamp) '
			+ 'VALUES ('
				+ '(SELECT id FROM karma WHERE item = $item)'
				+ ', $delta, $reason, $user, $timestamp)';

		var values = {
			$item: item
			, $delta: delta
			, $reason: reason
			, $user: user
			, $timestamp: Context.moment().utc().valueOf()
		};

		Context.maindb.run(query, values, function (err) {
			if (err != null) {
				Context.Log.error(
					'Error adding reason for item "%s": %s'
					, item
					, err.message);
			}
		});
	};

	var sendUpdate = function (recv, item, karma, delta) {
		dAmn.sendMsg(
			recv.channel
			, Context.fmt(
				'%s %s karma %s "%s", bringing it to %d.'
				, recv.from
				, (delta < 0) ? 'took' : 'gave'
				, (delta < 0) ? 'from' : 'to'
				, item
				, karma));
	};

	var floodWarning = function (recv, item) {
		dAmn.sendMsg(
			recv.channel
			, Context.fmt(
				'Karma for "%s" has already been modified in '
				+ 'the last %d minutes. Try again later.'
				, item
				, karmaChangeLimit / 60000));
	};

	var updateItem = function (id, item, karma, fn) {
		var query =
			'REPLACE INTO karma '
			+ '(id, item, karma, last_modified) '
			+ 'VALUES ($id, $item, $karma, $last_modified)';

		var values = {
			$id: id
			, $item: item
			, $karma: karma
			, $last_modified: Context.moment().utc().valueOf()
		};

		Context.maindb.run(query, values, fn);
	};

	var selectItem = function (item, fn) {
		var query =
			'SELECT * '
			+ 'FROM karma '
			+ 'WHERE item = $item';

		var values = {$item: item};
		Context.maindb.get(query, values, fn);
	};

	var modifyKarma = function (recv, delta, item, reason) {
		selectItem(item, function (err, row) {
			if (err != null) {
				transactionLock.release();
				Context.Log.error(
					'Error retrieving karma information for "%s": %s'
					, item
					, err.message);

				return;
			}

			var newKarma = null;
			var itemID = null;

			if (row == null) {
				newKarma = delta;
			} else {
				itemID = row.id;
				var now = Context.moment().utc().valueOf();
				var diff = now - row.last_modified;

				if (diff < karmaChangeLimit) {
					transactionLock.release();
					floodWarning(recv, item);
					return;
				}

				newKarma = row.karma + delta;
			}

			updateItem(itemID, item, newKarma, function (err) {
				transactionLock.release();
				if (err != null) {
					Context.Log.error(
						'Error updating karma value for "%s": %s'
						, item
						, err.message);

					return;
				}

				sendUpdate(recv, item, newKarma, delta);

				if (reason != null) {
					addReason(item, delta, reason, recv.from);
				}
			});
		});
	};

	var extractItemAndReason = function (searchString) {
		var item = null;
		var reason = null;

		// Try to see if we're dealing with a complex item surrounded by quotes.
		var matches = searchString.match(/^"([^"]+)"/);
		if (matches === null || matches[1] == null) {
			// Assume it's a simple item and we can just take the first word.
			var spacePos = searchString.indexOf(' ');
			if (spacePos < 0) {
				item = searchString;
			} else {
				item = searchString.substring(0, spacePos);
				reason = searchString.substring(spacePos + 1);

				if (reason.length < 1) {
					reason = null;
				}
			}
		} else {
			item = matches[1];

			// Add 3 for the two quotation characters and one space character.
			reason = searchString.substring(item.length + 3);
			if (reason.length < 1) {
				reason = null;
			}
		}

		return [item, reason];
	};

	Context.HelpText.givekarma =
		'Usage:\n'
		+ '	!givekarma "<b>item</b>" <b>reason</b> - Give one point of karma '
		+ 'to the specified item. This may be a single word or phrase but if '
		+ 'it contains spaces, it must be surrounded by double quotes (") in '
		+ 'order to be recorded properly. You may optionally specify a reason '
		+ 'for this karma change. As a shortcut to this command you may use '
		+ '"<b>item</b>"++.';

	EventProcessor.register('cmd_givekarma', function (recv) {
		var searchString = Context.Packet.hasArg(recv);
		if (!searchString) {
			return;
		}

		var itemAndReason = extractItemAndReason(searchString);
		var item = itemAndReason[0];
		var reason = itemAndReason[1];
		var delta = 1;

		if (item.toLowerCase() === recv.from.toLowerCase()) {
			delta = -1;
		}

		transactionLock.acquire(modifyKarma.curry(recv, delta, item, reason));
	});

	Context.HelpText.takekarma =
		'Usage:\n'
		+ '	!takekarma "<b>item</b>" <b>reason</b> - Take one point of karma '
		+ 'from the specified item. This may be a single word or phrase but if '
		+ 'it contains spaces, it must be surrounded by double quotes (") in '
		+ 'order to be recorded properly. You may optionally specify a reason '
		+ 'for this karma change. As a shortcut to this command you may use '
		+ '"<b>item</b>"--.';

	EventProcessor.register('cmd_takekarma', function (recv) {
		var searchString = Context.Packet.hasArg(recv);
		if (!searchString) {
			return;
		}

		var itemAndReason = extractItemAndReason(searchString);
		transactionLock.acquire(
			modifyKarma.curry(recv, -1, itemAndReason[0], itemAndReason[1]));
	});

	Context.HelpText.karma =
		'Usage:\n'
		+ '	!karma <b>item</b> - Display the current karma score for the '
		+ 'specified item.';

	EventProcessor.register('cmd_karma', function (recv) {
		var searchString = Context.Packet.hasArg(recv);
		if (!searchString) {
			return;
		}

		var item = searchString.replace(/"/g, '');
		var query =
			'SELECT karma '
			+ 'FROM karma '
			+ 'WHERE item = $item';

		var values = {$item: item};

		Context.maindb.get(query, values, function (err, row) {
			if (err != null) {
				Context.Log.error(
					'Unable to retrieve karma for item "%s": %s'
					, item
					, err.message);

				return;
			}

			var karma = 0;
			if (row != null && row.karma != null) {
				karma = row.karma;
			}

			dAmn.sendMsg(
				recv.channel
				, Context.fmt('"%s" has %d karma.', item, karma));
		});
	});

	EventProcessor.register('recvMsg', function (recv) {
		if (recv.content == null || recv.content.length < 1) {
			return;
		}

		// Ignore own messages.
		if (recv.from.toLowerCase() === Context.config.name.toLowerCase()) {
			return;
		}

		var quotesLength = 0;
		var matches = recv.content.match(/^"([^"]+)"(\+\+|--)/);

		if (matches === null) {
			matches = recv.content.match(/^(\w+)(\+\+|--)( |$)/);
			if (matches === null) {
				return;
			}
		} else {
			quotesLength = 2;
		}

		if (matches[1] == null || matches[2] == null) {
			return;
		}

		var item = matches[1];
		var delta = (matches[2] === '++') ? 1 : -1;
		var reason = recv.content.substring(item.length + quotesLength + 3);

		if (reason.length < 1) {
			reason = null;
		}

		transactionLock.acquire(modifyKarma.curry(recv, delta, item, reason));
	});
};
