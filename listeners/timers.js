'use strict';

module.exports = function (Context, EventProcessor, dAmn) {
	var calculateRelativeTime = function (args) {
		var allUndefined = true;
		var now = Context.moment().utc();
		var n = NaN;

		for (var i = 0; i < 8; i++) {
			if (args[i] === undefined) {
				continue;
			}

			allUndefined = false;

			if (i % 2 === 0) {
				n = parseInt(args[i]);
				if (isNaN(n)) {
					return null;
				}
			} else {
				switch (args[i]) {
					case 'd': now.add(n, 'days');    break;
					case 'h': now.add(n, 'hours');   break;
					case 'm': now.add(n, 'minutes'); break;
					case 's': now.add(n, 'seconds'); break;
					default : return null;
				}
			}
		}

		if (allUndefined || isNaN(n)) {
			return null;
		}

		return now.valueOf();
	};

	var calculateAbsoluteTime = function (words) {
		var now = Context.moment().utc();
		var regexTime = /^\d{2}:\d{2}(:\d{2})?/;
		var regexSlashesDate = /^\d{2}\/\d{2}(\/\d{4})?/;
		var regexDashesDate = /^(\d{4}-)?\d{2}-\d{2}/;

		if (!regexTime.test(words[0])
			&& !regexSlashesDate.test(words[0])
			&& !regexDashesDate.test(words[0])) {

			return null;
		}

		// Because JSLint is complaining about redefinitions.
		var dateComponents;
		var timeComponents;

		var parseTime = function (word) {
			timeComponents = word.split(':');
			now.hours(timeComponents[0]);
			now.minutes(timeComponents[1]);

			if (timeComponents.length > 2) {
				now.seconds(timeComponents[2]);
			}
		};

		if (regexTime.test(words[0])) {
			parseTime(words[0]);
		} else if (regexSlashesDate.test(words[0])) {
			dateComponents = words[0].split('/');
			now.date(dateComponents[0]);
			now.month(parseInt(dateComponents[1]) - 1);

			if (dateComponents.length > 2) {
				now.year(dateComponents[2]);
			}

			if (regexTime.test(words[1])) {
				parseTime(words[1]);
			}
		} else if (regexDashesDate.test(words[0])) {
			dateComponents = words[0].split('-');
			if (dateComponents[0].length === 4) {
				now.year(dateComponents[0]);
				dateComponents.shift();
			}

			now.month(parseInt(dateComponents[0]) - 1);
			now.date(dateComponents[1]);

			if (regexTime.test(words[1])) {
				parseTime(words[1]);
			}
		}

		return now;
	};

	var executeTimer = function (channel, from, cmd, timerID) {
		// Delete the timer from the database since it has now run successfully.
		// This can be done in parallel with the actual execution of the timer.
		var query =
			'DELETE FROM timers '
			+ 'WHERE id = $id';

		var values = {$id: timerID};

		Context.maindb.run(query, values, function (err) {
			if (err != null) {
				Context.Log.error(
					'Error removing timer #%d from the database: %s'
					, timerID
					, err.message);
			}
		});

		var cmdVerb = cmd;
		var spacePos =  cmd.indexOf(' ');

		if (spacePos > -1) {
			cmdVerb = cmd.substring(0, spacePos);
		}

		var packet = {
			channel: channel
			, from: from
			, action: Context.Recv.Actions.MSG
			, content: Context.config.cmdChar[0] + cmd
		};

		EventProcessor.fire('cmd_' + cmdVerb.toLowerCase(), packet);
	};

	var setTimer = function (channel, from, timestamp, cmd, timerID) {
		var now = Context.moment().utc().valueOf();
		var diff = timestamp.valueOf() - now;

		setTimeout(executeTimer.curry(channel, from, cmd, timerID), diff);
	};

	var addTimer = function (recv, timestamp, cmd) {
		// First persist the timer in the database in case the bot crashes or is
		// disconnected before the timer fires.
		var query =
			'INSERT INTO timers '
			+ '(channel, `from`, timestamp, cmd) '
			+ 'VALUES ($channel, $from, $timestamp, $cmd)';

		var values = {
			$channel: recv.channel
			, $from: recv.from
			, $timestamp: timestamp.valueOf()
			, $cmd: cmd
		};

		Context.maindb.run(query, values, function (err) {
			if (err != null) {
				Context.Log.error(
					'Unable to store timer information in database: %j\n%s'
					, values
					, err.message);

				return;
			}

			setTimer(recv.channel, recv.from, timestamp, cmd, this.lastID);

			dAmn.sendMsg(
				recv.channel
				, Context.fmt(
					'%s: Timer set for %s.'
					, recv.from
					, Context.DateUtils.getPrettyDate(timestamp)));
		});
	};

	Context.HelpText.in = Context.fmt(
		"Usage:\n"
		+ "	!in <b>time</b> <b>command</b> - Perform the specified command "
		+ "after the specified amount of time has elapsed.\n\n"
		+ "Examples:\n"
		+ "	!in 1h50m say hello - Causes %s to write 'hello' in the current "
		+ "channel after 1 hour and 50 minutes have passed.\n"
		+ "	!in 14d10h20m50s reply get a haircut - Causes %s to reply "
		+ "specifically to you in the current channel with the message 'get a "
		+ "haircut' after 14 days, 10 hours, 20 minutes and 50 seconds have "
		+ "passed.\n"
		+ "	!in 10m buytopic woo~ a topic from the past - Attempts to buy the "
		+ "current topic with your balance after 10 minutes have passed."
		, Context.config.name
		, Context.config.name);

	EventProcessor.register('cmd_in', function (recv) {
		var searchString = Context.Packet.hasArg(recv);
		if (!searchString) {
			return;
		}

		var regex = /(\d+)?(d)?(\d+)?(h)?(\d+)?(m)?(\d+)?(s)? (.*)$/;
		var matches = searchString.match(regex);
		if (matches === null || matches[9] == null || matches[9].length < 1) {
			return;
		}

		matches.shift();
		var cmd = matches[8];
		var targetTime = calculateRelativeTime(matches);

		if (targetTime === null) {
			return;
		}

		addTimer(recv, targetTime, cmd);
	});

	Context.HelpText.at = Context.fmt(
		"Usage:\n"
		+ "	!at <b>time</b> <b>command</b> - Perform the specified command at "
		+ "the time specified in UTC.\n\n"
		+ "Examples:\n"
		+ "	!at 19:00 say hammertime - Causes %s to write 'hammertime' in "
		+ "the current channel at 19:00 UTC.\n"
		+ "	!at 31/01 reply good morning - Causes %s to reply to you "
		+ "specifically in the current channel with the message 'good morning' "
		+ "at 00:00 UTC on the 31st of January.\n"
		+ "	!at 2222-22-22 22:22:22 say welcome to the future!"
		, Context.config.name
		, Context.config.name);

	EventProcessor.register('cmd_at', function (recv) {
		var searchString = Context.Packet.hasArg(recv);
		if (!searchString) {
			return;
		}

		var words = searchString.split(' ');
		if (words.length < 2) {
			return;
		}

		var targetTime = calculateAbsoluteTime(words);
		var regexTime = /^\d{2}:\d{2}(:\d{2})?/;

		if (targetTime == null) {
			return;
		}

		words.shift();
		if (regexTime.test(words[0])) {
			words.shift();
		}

		if (words.length < 1) {
			return;
		}

		var cmd = words.join(' ');
		addTimer(recv, targetTime, cmd);
	});

	EventProcessor.register('init', function () {
		// Load any timers that are still pending since the bot last shut down.
		var query =
			'SELECT * '
			+ 'FROM timers';

		Context.maindb.all(query, function (err, rows) {
			if (err != null) {
				Context.Log.error(
					'Error loading pending timers on startup: %s'
					, err.message);

				return;
			}

			rows.forEach(function (row) {
				setTimer(row.channel, row.from, row.timestamp, row.cmd, row.id);
			});
		});
	});
};
