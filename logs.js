'use strict';

require('./lib/function.js');
var moment = require('moment');
var fs = require('fs');
var fmt = require('util').format;
var sqlite3 = require('sqlite3');
var wsServer = require('ws').Server;
var utils = require('./lib/file_utils.js');

var Log = require('./lib/log.js');
var Packet = require('./lib/packet.js');
var Cache = require('./lib/cache.js');
var Locks = require('./lib/locks.js');
var Pool = require('./lib/resource_pool.js');
var RefCount = require('./lib/refcount.js');
var FileUtils = require('./lib/file_utils.js');
var DistQuery = require('./lib/distributed_query.js');

Log = new Log(false);
Packet = new Packet({Log: Log});
Locks = new Locks({Log: Log, cmdLine: {debug: false}});
Pool = new Pool({Locks: Locks});

var getLogpath = function (day, dir) {
	if (dir != null && dir[dir.length - 1] !== '/') {
		dir += '/';
	}

	if (dir == null) {
		dir = '';
	}

	return fmt(
		'%s%s.%d.log.sq3.bz2'
		, dir
		, day.format('YYYY/MM/DD')
		, day.valueOf());
};

DistQuery = new DistQuery({
	fs: fs
	, sqlite3: sqlite3
	, moment: moment
	, cmdLine: {debug: false}
	, Log: Log
	, Cache: Cache
	, FileUtils: FileUtils
	, RefCount: RefCount
	, Locks: Locks
	, Pool: Pool
}, 'logs/', getLogpath);

var NUM_COLOURS = 203;

var conf = {logDirectory: 'logs/'};
var cache = {};
var loglines = 0;
var processStart = moment();

var username2Colour = function (username) {
	var total = 0;
	for (var i = 0; i < username.length; i++) {
		total += username.charCodeAt(i);
	}

	return 'colour' + (total % NUM_COLOURS);
};

var writeMsg = function (packet, date) {
	if (packet.args.from == null || packet.body == null) {
		return null;
	}

	return fmt(
		'<div class="row">'
		+	'<div class="col-xs-1"><strong>%s</strong></div>'
		+	'<div class="col-xs-11">'
		+		'<strong>&lt;<span class="%s">%s</span>&gt;</strong> %s'
		+	'</div>'
		+ '</div>'
		, date.format('HH:mm:ss')
		, username2Colour(packet.args.from)
		, packet.args.from
		, packet.body
	);
};

var writeJoin = function (packet, date) {
	if (packet.param == null) {
		return null;
	}

	return fmt(
		'<div class="row">'
		+	'<div class="col-xs-1"><strong>%s</strong></div>'
		+	'<div class="col-xs-11">'
		+		'<strong>** %s</strong> has joined.'
		+	'</div>'
		+ '</div>'
		, date.format('HH:mm:ss')
		, packet.param
	);
};

var writeAction = function (packet, date) {
	if (packet.args.from == null || packet.body == null) {
		return null;
	}

	return fmt(
		'<div class="row">'
		+	'<div class="col-xs-1"><strong>%s</strong></div>'
		+	'<div class="col-xs-11"><em><strong>* %s</strong> %s</em></div>'
		+ '</div>'
		, date.format('HH:mm:ss')
		, packet.args.from
		, packet.body
	);
};

var writePart = function (packet, date) {
	if (packet.param == null) {
		return null;
	}

	return fmt(
		'<div class="row">'
		+	'<div class="col-xs-1"><strong>%s</strong></div>'
		+	'<div class="col-xs-11"><strong>** %s</strong> has left.</div>'
		+ '</div>'
		, date.format('HH:mm:ss')
		, packet.param
	);
};

var writeKicked = function (packet, date) {
	if (packet.param == null || packet.args.by == null) {
		return null;
	}

	return fmt(
		'<div class="row">'
		+	'<div class="col-xs-1"><strong>%s</strong></div>'
		+	'<div class="col-xs-11">'
		+		'<strong>** %s was kicked by %s.</strong>'
		+	'</div>'
		+ '</div>'
		, date.format('HH:mm:ss')
		, packet.param
		, packet.args.by
	);
};

var writePacket = function (timestamp, packet) {
	var date = moment(timestamp).utc();
	var line = null;

	switch (packet.cmd) {
		case 'msg':    line = writeMsg(packet, date);    break;
		case 'join':   line = writeJoin(packet, date);   break;
		case 'action': line = writeAction(packet, date); break;
		case 'part':   line = writePart(packet, date);   break;
		case 'kicked': line = writeKicked(packet, date); break;
	}

	return line;
};

var generateHTML = function (options) {
	var writeHTMLHeader = function (htmlfile, filesWritten) {
		if (filesWritten[htmlfile] != null) {
			return;
		}

		filesWritten[htmlfile] = true;
		var html =
			'<html>'
			+	'<head>'
			+		'<link href="../../../bootstrap/css/bootstrap.min.css" '
			+		'type="text/css" rel="stylesheet">'
			+		'<link href="../../../'
			+		'bootstrap/css/bootstrap-theme.min.css" '
			+		'type="text/css" rel="stylesheet">'
			+		'<link href="../../../colours.css" type="text/css" '
			+		'rel="stylesheet">'
			+	'</head>'
			+	'<body>'
			+		'<div class="container">';

			fs.writeFileSync(htmlfile, html, {mode: 436});
	};

	var writeHTMLFooter = function (htmlfile) {
		var html =
					'</div>'
			+		'<script type="text/javascript" '
			+		'src="../../../bootstrap/js/jquery-2.1.1.min.js">'
			+		'</script>'
			+		'<script type="text/javascript" '
			+		'src="../../../bootstrap/js/bootstrap.min.js">'
			+		'</script>'
			+	'</body>'
			+ '</html>';

		fs.appendFileSync(htmlfile, html, {mode: 436});
	};

	var prepareSubDirectory = function (htmlDir, channel, htmlfile) {
		if (cache.dirs == null) {
			cache.dirs = {};
		}

		var splitOnSlash = htmlfile.split('/');
		var year = splitOnSlash[0];
		var month = splitOnSlash[1];

		if (year == null || month == null) {
			return false;
		}

		var channelDir = htmlDir + channel;
		var yearDir = fmt('%s/%s', channelDir, year);
		var monthDir = fmt('%s/%s', yearDir, month);

		[channelDir, yearDir, monthDir].forEach(function (dir) {
			if (cache.dirs[dir] == null) {
				cache.dirs[dir] = true;

				try {
					fs.mkdirSync(dir, 509);
				} catch (ignored) {}
			}
		});

		return fmt('%s/%s', channelDir, htmlfile);
	};

	var log2HTML = function (htmlDir, htmlfile, code, logfile) {
		if (code !== 0) {
			console.error('Unable to extract %s...', logfile);
			cleanup();
			return;
		}

		console.log('Processing %s...', logfile);
		var db = new sqlite3.Database(logfile);
		var filesWritten = {};

		db.on('error', function (err) {
			console.error(
				'Unable to open %s, no HTML will be generated for it: %s'
				, logfile
				, err.message);
		});

		db.on('open', function () {
			var query =
			'SELECT * '
			+ 'FROM log '
			+ 'ORDER BY timestamp ASC';

			db.each(query, function (err, row) {
				if (err != null) {
					console.error('Error processing log line: %s', err.message);
				}

				if (row.subpacket == null || row.channel == null) {
					return;
				}

				var htmlpath =
					prepareSubDirectory(htmlDir, row.channel, htmlfile);

				if (!htmlpath) {
					return;
				}

				var packet = Packet.parse(row.subpacket);
				writeHTMLHeader(htmlpath, filesWritten);
				var line = writePacket(row.timestamp, packet);

				if (line != null) {
					fs.appendFileSync(htmlpath, line, {mode: 436});
				}
			}, done);
		});

		var done = function (err, rowCount) {
			if (err != null) {
				console.error(
					'An error occurred when processing log file %s: %s'
					, logfile
					, err.message);
			}

			loglines += rowCount;
			for (var file in filesWritten) {
				writeHTMLFooter(file);
			}

			cleanup();
		};
	};

	var cleanup = function () {
		processed++;
		if (processed >= endDay.diff(startDay, 'days')) {
			utils.deleteDir(tmpDir, function (code) {
				if (code !== 0) {
					console.error(
						'There was an error deleting the %s directory, '
						+ 'please do so manually.'
						, tmpDir);
				}
			});
		}
	};

	if (options.startDay == null || options.endDay == null) {
		printHelp();
		process.exit(1);
	}

	// Make sure the dates are in UTC before parsing them.
	var startDay = moment(options.startDay + 'T00:00:00Z');
	var endDay = moment(options.endDay + 'T00:00:00Z');

	// valueOf() is NaN if the date couldn't be parsed.
	if (isNaN(startDay.valueOf()) || isNaN(endDay.valueOf())) {
		printHelp();
		process.exit(1);
	}

	if (startDay > endDay) {
		var tmp = endDay;
		endDay = startDay;
		startDay = tmp;
	}

	endDay.add(1, 'day');

	// Create tmp/ and html/. If they already exist we just ignore the error.
	var tmpDir = 'tmp/';
	var htmlDir = 'html/';

	try {
		fs.mkdirSync(tmpDir, 509); // Chmod 0775
	} catch (ignored) {}

	var currentDay = moment(startDay).utc();
	var processed = 0;

	var doCopy = function (tmpfile, htmlDir, htmlfile, err) {
		if (err == null) {
			utils.unzip(tmpfile, log2HTML.curry(htmlDir, htmlfile));
		} else {
			processed++;
		}
	};

	// Loop over each of the days until we get to endDay.
	while (endDay.diff(currentDay, 'days') > 0) {
		var logfile = getLogpath(currentDay, conf.logDirectory);
		var htmlfile = fmt(
			'%s.%d.log.html'
			, currentDay.format('YYYY/MM/DD')
			, currentDay.valueOf());

		var tmpfile = fmt(
			'%s%s.%d.log.sq3.bz2'
			, tmpDir
			, currentDay.format('DD')
			, currentDay.valueOf());

		utils.copyFile(
			logfile
			, tmpfile
			, doCopy.curry(tmpfile, htmlDir, htmlfile));

		currentDay.add(1, 'day');
	}

	process.on('exit', function (code) {
		var processEnd = moment();
		console.log(
			'Done... %d lines processed in %dms.'
			, loglines
			, processEnd.diff(processStart, 'milliseconds'));
	});
};

var launchServer = function (options) {
	var port = 8080;
	if (options.port != null) {
		var portParseAttempt = parseInt(options.port);
		if (!isNaN(portParseAttempt)) {
			port = portParseAttempt;
		}
	}

	var sendError = function (client, msg) {
		client.send(fmt('<error>%s</error>', msg), function (err) {
			if (err != null) {
				console.error(err.message);
			}
		});
	};

	var writeResults = function (days, data) {
		var html = '';

		for (var i = 0; i < days.length; i++) {
			var day = days[i];
			var datum = data[i];

			if (datum.length < 1) {
				continue;
			}

			html += fmt(
				'<div class="container date-block">'
				+	'<div class="row">'
				+		'<div class="col-md-12"><h3>%s</h3></div>'
				+	'</div>'
				+ '</div>'
				, moment(day).utc().format('DD/MM/YYYY'));

			html += '<div class="container log-block">';

			for (var n = 0; n < datum.length; n++) {
				var row = datum[n];
				var packet = Packet.parse(row.subpacket);
				var line = writePacket(row.timestamp, packet);

				if (line != null) {
					html += line;
				}
			}

			html += '</div>';
		}

		if (html === '') {
			return '<div class="container"><div class="row">'
				+ '<div class="col-md-12"><h1>No results.</h1>'
				+ '</div></div></div>';
		}

		return html;
	};

	var server = new wsServer({port: port});
	server.on('connection', function (client) {
		client.on('message', function (msg) {
			var options = null;
			try {
				options = JSON.parse(msg);
			} catch (e) {
				sendError(client, e.message);
				return;
			}

			if (options.start == null
				|| options.end == null
				|| options.query == null) {

				sendError(client, 'Missing options for distributed query.');
				return;
			}

			var totalDays =
				moment(options.end).utc().startOf('day').diff(
					moment(options.start).utc().startOf('day'), 'days');

			options.type = DistQuery.QueryType.ALL;
			options.mode = DistQuery.QueryMode.ORDERED;

			var data = [];
			var days = [];

			options.outputFn = function (day, err, rows) {
				if (err != null) {
					console.log(err.message);
					return;
				}

				data.push(rows);
				days.push(day.valueOf());

				// TODO: We can generate HTML in between getting data so no need
				// to store it all first. For now this is fast enough.
				if (days.length >= totalDays) {
					var html = writeResults(days, data);
					client.send(html, function (err) {
						if (err != null) {
							console.log(err);
						}
					});
				}
			};

			var errors = DistQuery.query(options);
			if (errors != null) {
				for (var i = 0; i < errors.length; i++) {
					console.log(errors[i]);
				}
			}
		});
	});
};

var printHelp = function () {
	console.log(
		'Usage: node logs.js COMMAND OPTIONS\n'
		+ 'Commands:\n'
		+ '	html\n'
		+ '	server\n\n'
		+ 'Options:\n'
		+ '	--day=YYYY-MM-DD\n'
		+ '	--start-day=YYYY-MM-DD\n'
		+ '	--end-day=YYYY-MM-DD\n'
		+ '	--port=X\n\n'
		+ 'See README.md for more details.'
	);
};

var parseCmdLineArgs = function (args) {
	var help = false;
	var cmd = null;
	var options = {};

	// The first argument has to be the command otherwise it's an error.
	cmd = args.shift();

	if (cmd == null) {
		cmd = '';
	}

	// Map the command string to some function or throw the help message if we
	// don't have a function to associate with that string.
	switch (cmd.toLowerCase()) {
		case 'html':   cmd = Commands.HTML;   break;
		case 'server': cmd = Commands.SERVER; break;

		default: help = true;
	}

	for (var i = 0; i < args.length; i++) {
		// If the argument isn't of the form --option=value then value will
		// simply be undefined which the later functions should check for.
		var splitOnEquals = args[i].split('=');
		var option = splitOnEquals[0];
		var value = splitOnEquals[1];

		switch (option) {
			case '--help': help = true; break;
			case '--start-day': options.startDay = value; break;
			case '--end-day': options.endDay = value; break;
			case '--day':
				options.startDay = value;
				options.endDay = value;
				break;

			case '--port': options.port = value; break;

			default:
				console.error('Unrecognised option: %s...', option);
		}
	}

	if (help) {
		printHelp();
		process.exit(0);
	}

	cmd(options);
};

try {
	conf = JSON.parse(fs.readFileSync('conf.json'));
} catch (e) {
	console.error('Could not read conf.json: %s', e.message);
}

var Commands = Object.freeze({
	HTML: generateHTML
	, SERVER: launchServer
});

// Remove the 'node' and 'logs.js' from the argv array.
var args = process.argv;
args.shift();
args.shift();
parseCmdLineArgs(args);
