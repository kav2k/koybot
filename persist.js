'use strict';

var fork = require('child_process').fork;
var tui = require('./lib/tui.js');

var exit = false;
var useTUI = false;
var currentChild = null;
var lastConnectAttempt = null;
var connectSpamThreshold = 2000;
var backoff = 1;

var args = process.argv;
args.shift();
args.shift();

for (var i = 0; i < args.length; i++) {
	if (args[i] === '--tui') {
		useTUI = true;
	}
}

try {
	JSON.parse(require('fs').readFileSync('conf.json'));
} catch (e) {
	console.error(
		'Invalid config file. Please copy conf-example.json '
		+ 'to conf.json and edit it appropriately.');
	process.exit(1);
}

var receive = function (input) {
	// We have received some command line input.
	if (currentChild == null) {
		return;
	}

	currentChild.send(input);
};

var linearBackoff = function () {
	var currentTime = new Date().getTime();
	var reconnectGap = currentTime - lastConnectAttempt;

	if (reconnectGap >= 0 && reconnectGap < connectSpamThreshold) {
		// We don't want to spam reconnects to wait before spawning again.
		backoff++;
		lastConnectAttempt = null;

		setTimeout(spawn, backoff * connectSpamThreshold);

		return false;
	}

	return true;
};

var spawn = function () {
	if (lastConnectAttempt == null) {
		lastConnectAttempt = new Date().getTime();
	} else if (linearBackoff()) {
		lastConnectAttempt = new Date().getTime();
		backoff = 1;
	} else {
		return;
	}

	var child = fork('koy.js', args);
	currentChild = child;

	child.on('message', function (msg) {
		console.log(msg);
	});

	child.on('error', function (err) {
		console.error('Error spawning bot: ' + err.message);
		process.exit(1);
	});

	child.on('exit', function (code, signal) {
		if (exit) {
			if (useTUI) {
				tui.close();
			}

			process.stdout.write('Bot killed succesfully, shutting down.\n');
			process.exit();
		}

		console.log(
			'Bot encountered an error or connection was closed. Restarting.');
		spawn();
	});
};

var forceShutdown = function () {
	console.log('Bot took too long to shutdown, killing.');
	currentChild.kill();
	process.exit();
};

if (useTUI) {
	tui = new tui(receive);
	console.log = tui.write;
	console.error = tui.write;
}

console.log('Starting bot.');
spawn();

process.on('SIGINT', function () {
	console.log('Sending SIGINT signal to bot.');

	exit = true;
	currentChild.kill('SIGINT');

	setTimeout(forceShutdown, 10000);
});
