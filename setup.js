'use strict';

var Context = {};
var readline = require('readline');
var fmt = require('util').format;

Context.sqlite3 = require('sqlite3');
Context.fs = require('fs');
Context.https = require('https');

var DatabaseLoader = require('./lib/database_loader.js');
var Log = require('./lib/log.js');
var admin = require('./admin.js');

Context.Log = new Log(false);
var loader = new DatabaseLoader(Context);

var done = function () {
	Context.Log.log(
		'\nSetup is now complete. You can now run the bot with '
		+ 'the command `node persist.js` or consult the README '
		+ 'for more detailed instructions. Please make sure you '
		+ 'have edited conf.json appropriately.');
};

var promptForAuthorizationCode = function (oAuthFD, clientID, clientSecret) {
	Context.Log.log(
		'In order for your bot to make calls to dA\'s API, you must '
		+ 'login as your bot and visit the following link in your browser:\n'
		+ 'https://www.deviantart.com/oauth2/authorize'
		+ '?response_type=code'
		+ '&client_id=%s'
		+ '&redirect_uri=http://kim.mantas.me.uk/koybot/auth/'
		+ '&scope=basic\n\n'
		, clientID);

	var userInput = readline.createInterface({
		input: process.stdin
		, output: process.stdout
	});

	userInput.on('SIGINT', userInput.close);
	userInput.on('SIGTSTP', userInput.close);

	userInput.question('Enter the code you received here: ', function (code) {
		Context.Log.log('Attempting to validate code...');
		var url = fmt(
			'https://www.deviantart.com/oauth2/token?'
			+ 'grant_type=authorization_code'
			+ '&redirect_uri=http://kim.mantas.me.uk/koybot/auth/'
			+ '&client_id=%s'
			+ '&client_secret=%s'
			+ '&code=%s'
			, clientID
			, clientSecret
			, code);

		var req = Context.https.get(url, function (res) {
			var data = '';

			res.on('data', function (d) {
				data += d;
			});

			res.on('end', function () {
				var store = {};

				try {
					store = JSON.parse(data);
				} catch (e) {
					Context.Log.error(
						'The data received was not a valid OAuth response: %s. '
						+ 'Please try again.'
						, e.message);

					userInput.close();
					promptForAuthorizationCode(oAuthFD, clientID, clientSecret);
					return;
				}

				if (store.expires_in == null
					|| store.access_token == null) {

					Context.Log.error(
						'OAuth token response corrupt: %s. Please try again.'
						, data);

					userInput.close();
					promptForAuthorizationCode(oAuthFD, clientID, clientSecret);
					return;
				}

				var totalToWrite = data.length;
				var totalWritten = 0;
				while (totalWritten < totalToWrite) {
					var written = Context.fs.writeSync(
						oAuthFD
						, data
						, totalWritten
						, totalToWrite
						, null);

					totalWritten += written;

					if (written === 0) {
						break;
					}
				}

				try {
					Context.fs.closeSync(oAuthFD);
				} catch (ignored) {}

				done();
			});
		});

		req.on('error', function (err) {
			Context.Log.error(
				'There was an error downloading the OAuth data. '
				+ 'Please try again.');

			userInput.close();
			promptForAuthorizationCode(oAuthFD, clientID, clientSecret);
		});
	});
};

var setupOAuth = function () {
	// Check for config data.
	Context.fs.readFile('conf.json', {encoding: 'utf8'}, function (err, data) {
		var conf = null;
		if (err == null) {
			try {
				conf = JSON.parse(data);
			} catch (e) {
				Context.Log.error(
					'There was an error trying to parse the config file: %s. '
					+ 'Setup cannot continue.'
					, e.message);

				return;
			}
		}

		var oAuthFile = 'oauth.json';
		if (conf.oAuthStore != null) {
			oAuthFile = conf.oAuthStore;
		}

		if (conf.oAuthClientID == null) {
			Context.Log.error(
				'Unable to determine OAuth client ID. Setup cannot continue.');

			return;
		}

		if (conf.oAuthClientSecret == null) {
			Context.Log.error(
				'Unable to determine OAuth client secret. '
				+ 'Setup cannot continue.');

			return;
		}

		Context.fs.open(oAuthFile, 'w', 436, function (err, fd) {
			if (err != null) {
				Context.Log.error(
					'There was an error while opening "%s" to save the OAuth '
					+ 'information to: %s. Setup cannot continue.'
					, oAuthFile
					, err.message);

				return;
			}

			promptForAuthorizationCode(
				fd
				, conf.oAuthClientID
				, conf.oAuthClientSecret);
		});
	});
};

var setupName = function () {
	Context.fs.readFile('conf.json', {encoding: 'utf8'}, function (err, data) {
		var conf = null;
		if (err == null) {
			try {
				conf = JSON.parse(data);
			} catch (e) {
				Context.Log.error(
					'Unable to open conf.json. It may not exist or '
					+ 'could be corrupt. Please delete the file if '
					+ 'it exists and re-run setup.js in order to recreate it.');
			}
		} else {
			Context.Log.error(
				'Unable to open conf.json. It may not exist or '
				+ 'could be corrupt. Please delete the file if '
				+ 'it exists and re-run setup.js in order to recreate it.');
		}

		if (conf !== null) {
			Context.Log.log(
				'Before setup can continue, you must create a '
				+ 'new deviantART account for your bot. Once you '
				+ 'have done so, please enter its name exactly below.\n');

			var userInput = readline.createInterface({
				input: process.stdin
				, output: process.stdout
			});

			userInput.on('SIGINT', userInput.close);
			userInput.on('SIGTSTP', userInput.close);

			userInput.question('Bot name: ', function (name) {
				userInput.close();
				if (name.length < 1) {
					setupName();
				} else {
					conf.name = name;
					Context.fs.writeFile(
						'conf.json'
						, JSON.stringify(conf, null, '\t')
						, {encoding: 'utf8', mode: 436}
						, function (err) {
							if (err == null) {
								Context.Log.log(
									'\nName set to "%s". '
									+ 'Setting up OAuth.'
									, name);

								setupOAuth();
							} else {
								Context.Log.error(
									'Unable to write conf.json. Check you have '
									+ 'permission before trying again.');
								setupName();
							}
						}
					);
				}
			});
		}
	});
};

var adminAddSuccess = function (err, who) {
	if (err == null) {
		Context.Log.log('"%s" added successfully.', who);
	} else {
		Context.Log.error(
			'There was an error adding "%s" to the database: %s. '
			+ 'Setup will continue. If you want to try again, please run '
			+ 'setup again or consult the README on how to use admin.js.'
			, who
			, err.message);
	}

	setupName();
};

var promptForAdmin = function (db) {
	var userInput = readline.createInterface({
		input: process.stdin
		, output: process.stdout
	});

	userInput.on('SIGINT', userInput.close);
	userInput.on('SIGTSTP', userInput.close);

	userInput.question('Enter an admin username: ', function (answer) {
		userInput.question(
			fmt('Are you sure you want to add "%s" '
				+ 'as a bot administrator? [y/N] '
				, answer)
			, function (yesno) {
				userInput.close();
				yesno = yesno.toLowerCase();
				if (yesno === 'y' || yesno === 'yes') {
					admin.add(db, answer, adminAddSuccess);
				} else {
					promptForAdmin();
				}
			});
	});
};

var setupAdmin = function () {
	loader.load('db/main.sq3', 'db/main.sql', function (db) {
		Context.Log.log('Checking for an admin account...');

		if (db == null) {
			Context.Log.error(
				'Unable to open db/main.sq3. '
				+ 'Setup cannot continue without this database.');

			return;
		}

		db.get('select count(*) from admin', function (err, row) {
			if (err != null) {
				Context.Log.error(
					'Unable to check if the admin table is populated: "%s". '
					+ 'Setup will continue however the bot may not function '
					+ 'correctly without an admin user.'
					, err.message);

				setupName();
				return;
			}

			if (row == null || row['count(*)'] == null) {
				Context.Log.error(
					'Invalid row returned by the database: %j. '
					+ 'Setup will continue however the bot may not '
					+ 'function correctly without an admin user.'
					, row);

				setupName();
				return;
			}

			if (row['count(*)'] > 0) {
				Context.Log.log('Found.');
				setupName();
			} else {
				promptForAdmin(db);
			}
		});
	});
};

Context.Log.log('Checking for conf.json...');
Context.fs.stat('conf.json', function (err, stats) {
	if (err == null) {
		Context.Log.log('Found.');
		setupAdmin();
	} else {
		Context.Log.log('Not found. Copying conf-example.json to conf.json...');
		var read = Context.fs.createReadStream('conf-example.json');
		var write = Context.fs.createWriteStream('conf.json');

		var error = function (err) {
			Context.Log.error(
				'Unable to copy conf-example.json to conf.json: "%s". '
				+ 'Setup cannot continue.'
				, err.message);
		};

		read.on('error', error);
		write.on('error', error);
		write.on('finish', setupAdmin);
		read.pipe(write);
	}
});
