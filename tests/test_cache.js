'use strict';

var Cache = require('../lib/cache.js');

exports.testCache = function (test) {
	test.expect(7);

	var cache = Cache.create(2);
	var checkedPop = false;

	cache.on('pop', function (key, value) {
		if (checkedPop) {
			return;
		}

		test.strictEqual(key, 'key');
		test.strictEqual(value, 'value');
		checkedPop = true;
	});

	test.strictEqual(cache.get('404'), undefined);

	cache.put('key', 'value');
	test.strictEqual(cache.get('key'), 'value');

	cache.put('key2', 'value');
	cache.put('key3', 'value');
	test.strictEqual(cache.get('key'), undefined);

	setTimeout(function () {
		cache.get('key2');
		cache.put('key4', 'value');
		test.strictEqual(cache.get('key2'), 'value');
		test.strictEqual(cache.get('key3'), undefined);
		test.done();
	}, 1);
};
