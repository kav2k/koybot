'use strict';

require('../lib/function.js');
var q = require('../lib/distributed_query.js');
var testUtils = require('../lib/test_utils.js');
var fmt = require('util').format;

var getMockContext = function (attach) {
	var Context = {cmdLine: {}, config: {}};
	testUtils.interceptLogging(Context, attach);

	var Locks = require('../lib/locks.js');
	var Pool = require('../lib/resource_pool.js');

	Context.fs = {};
	Context.fs.mkdirSync = function () {};

	Context.sqlite3 = {};
	Context.sqlite3.Database = function () {
		return {
			on: function (evt, fn) {
				if (evt === 'open') {
					fn();
				}
			}

			, run: function (query, values, fn) {
				fn(null);
			}
		};
	};

	Context.Cache = {};
	Context.Cache.create = function () {
		return {
			on: function () {}
			, put: function () {}
			, get: function () {
				return 1;
			}

		};
	};

	Context.FileUtils = {};
	Context.FileUtils.unzip = function (archive, fn) {
		fn(0);
	};

	Context.FileUtils.copyFile = function (src, dest, fn) {
		fn();
	};

	Context.moment = require('moment');
	Context.fmt = fmt;
	Context.RefCount = require('../lib/refcount.js');
	Context.Locks = new Locks(Context);
	Context.Pool = new Pool(Context);

	return Context;
};

exports.testInsufficientArguments = function (test) {
	test.expect(3);

	try {
		new q();
	} catch (e) {
		test.strictEqual(
			e.message
			, 'Insufficient arguments to instantiate class.');
	}

	try {
		new q({});
	} catch (e) {
		test.strictEqual(
			e.message
			, 'Insufficient arguments to instantiate class.');
	}

	try {
		new q({}, '');
	} catch (e) {
		test.strictEqual(
			e.message
			, 'Insufficient arguments to instantiate class.');
	}

	test.done();
};

exports.testConfigOverride = function (test) {
	test.expect(4);

	var Context = getMockContext(this);

	Context.fs.mkdirSync = function (dir) {
		test.strictEqual(dir, 'cache/');
	};

	Context.Cache.create = function (size) {
		test.strictEqual(size, 128);
		return {on: function () {}};
	};

	new q(Context, '', function () {});

	Context.config.distQueryMaxCacheSize = 1;
	Context.config.distQueryCacheDirectory = 'a/';

	Context.fs.mkdirSync = function (dir) {
		test.strictEqual(dir, 'a/');
	};

	Context.Cache.create = function (size) {
		test.strictEqual(size, 1);
		return {on: function () {}};
	};

	new q(Context, '', function () {});

	test.done();
};

exports.testErrorSetQueryRange = function (test) {
	test.expect(2);

	var Context = getMockContext(this);
	var distQuery = new q(Context, '', function () {});

	var errors = distQuery.setQueryRange('????-??-??', '2000-01-01');
	test.strictEqual(errors[0], 'Cannot set start date to "????-??-??".');

	errors = distQuery.setQueryRange('????-??-??', '????-??-??');
	test.strictEqual(errors[1], 'Cannot set end date to "????-??-??".');

	test.done();
};

exports.testErrorQueryType = function (test) {
	test.expect(1);

	var Context = getMockContext(this);
	var distQuery = new q(Context, '', function () {return 'a/b/c.sq3.bz2';});
	var errors = distQuery.query({
		start: '2000-01-01'
		, end: '2000-01-01'
		, type: 0
	});

	test.strictEqual(errors[0], 'Unknown query type: 0');
	test.done();
};

exports.testErrorSetSpecificQueryRange = function (test) {
	test.expect(2);

	var Context = getMockContext(this);
	var today = Context.moment().utc().startOf('day');

	var distQuery = new q(Context, '', function () {return 'a/b/c.sq3.bz2';});
	var errors = distQuery.query({
		start: '????-??-??'
		, end: Context.moment(today).subtract(14, 'days')
		, type: 0
	});

	test.strictEqual(
		errors[0]
		, 'Cannot set query specific start date "????-??-??".');

	errors = distQuery.query({
		start: '????-??-??'
		, end: '????-??-??'
		, type: 0
	});

	test.strictEqual(
		errors[1]
		, 'Cannot set query specific end date "????-??-??".');

	test.done();
};

exports.testQuery = function (test) {
	test.expect(3);

	var Context = getMockContext(this);
	Context.Cache.create = function () {
		return {
			on: function () {}
			, put: function () {}
			, get: function () {
				return;
			}
		};
	};

	var distQuery = new q(Context, '', function () {return 'a/b/c.sq3.bz2';});
	distQuery.setQueryRange('2000-01-01', '2000-01-01');
	distQuery.run(null, {}, function (timestamp, err, out) {
		test.strictEqual(
			timestamp
			, Context.moment('2000-01-01').utc().startOf('day').valueOf());

		test.strictEqual(err, null);
		test.strictEqual(out, undefined);
	});

	test.done();
};

exports.testQueryOrderedFailures = function (test) {
	test.expect(3);

	var Context = getMockContext(this);

	Context.Cache.create = function () {
		return {
			on: function () {}
			, put: function () {}
			, get: function () {
				return;
			}
		};
	};

	Context.sqlite3.Database = function () {
		return {
			on: function (evt, fn) {
				if (evt === 'open' && count !== 1) {
					fn();
				} else if (evt !== 'open' && count === 1) {
					if (!thrownError) {
						thrownError = true;
						fn({message: 'bad'});
					} else {
						count++;
					}
				}
			}

			, run: function (query, values, fn) {
				fn(null);
			}
		};
	};

	var distQuery = new q(Context, '', function () {return 'a/b/c.sq3.bz2';});
	var count = 0;
	var thrownError = false;

	distQuery.query({
		start: '2000-01-01'
		, end: '2000-01-03'
		, mode: distQuery.QueryMode.ORDERED
		, type: distQuery.QueryType.EXEC
		, values: {}
		, outputFn: function (timestamp, err, out) {
			if (count === 1) {
				return;
			}

			test.strictEqual(
				timestamp
				, Context.moment('2000-01-01')
					.utc()
					.startOf('day')
					.add(count, 'days').valueOf());

			count++;
		}
	});

	test.strictEqual(
		this.error
		, 'There was an error running the query for 02/01/2000: bad');

	test.done();
};

exports.testQueryOrdered = function (test) {
	test.expect(3);

	var Context = getMockContext(this);

	Context.Cache.create = function () {
		return {
			on: function () {}
			, put: function () {}
			, get: function () {
				return;
			}
		};
	};

	var distQuery = new q(Context, '', function () {return 'a/b/c.sq3.bz2';});
	var count = 0;

	distQuery.query({
		start: '2000-01-01'
		, end: '2000-01-03'
		, mode: distQuery.QueryMode.ORDERED
		, type: distQuery.QueryType.EXEC
		, values: {}
		, outputFn: function (timestamp, err, out) {
			test.strictEqual(
				timestamp
				, Context.moment('2000-01-01')
					.utc()
					.startOf('day')
					.add(count, 'days').valueOf());

			count++;
		}
	});

	test.done();
};

exports.testIteratorQuery = function (test) {
	test.expect(50);

	var Context = getMockContext(this);

	Context.Cache.create = function () {
		return {
			on: function () {}
			, put: function () {}
			, get: function () {
				return;
			}
		};
	};

	Context.sqlite3.Database = function () {
		return {
			on: function (evt, fn) {
				if (evt === 'open') {
					fn();
				}
			}

			, each: function (query, fn, done) {
				for (var i = 0; i < 4; i++) {
					fn(null, callCount * 4 + i);
				}

				done(null, 4);
				callCount++;
			}
		};
	};

	var distQuery = new q(Context, '', function () {return 'a/b/c.sq3.bz2';});
	var callCount = 0;
	var outCount = 0;
	var doneCount = 0;

	var outputFn = function (timestamp, err, out) {
		test.strictEqual(out, outCount);

		if (outCount < 4) {
			test.strictEqual(
				timestamp
				, Context.moment('2000-01-01')
					.utc()
					.startOf('day')
					.valueOf());
		} else if (outCount < 8) {
			var d = '2000-01-02';
			if (thrownError) {
				d = '2000-01-03';
			}

			test.strictEqual(
				timestamp
				, Context.moment(d)
					.utc()
					.startOf('day')
					.valueOf());
		} else {
			test.strictEqual(
				timestamp
				, Context.moment('2000-01-03')
					.utc()
					.startOf('day')
					.valueOf());
		}

		outCount++;
	};

	var onComplete = function (timestamp, err, numRows) {
		if (doneCount === 1 && thrownError) {
			return;
		}

		test.strictEqual(numRows, 4);
		test.strictEqual(
			timestamp
			, Context.moment('2000-01-01')
				.utc()
				.startOf('day')
				.add(doneCount, 'days')
				.valueOf());

		doneCount++;
	};

	distQuery.query({
		start: '2000-01-01'
		, end: '2000-01-03'
		, type: distQuery.QueryType.EACH
		, mode: distQuery.QueryMode.ORDERED
		, outputFn: outputFn
		, onComplete: onComplete
	});

	callCount = 0;
	outCount = 0;
	doneCount = 0;
	var thrownError = false;
	Context.sqlite3.Database = function () {
		return {
			on: function (evt, fn) {
				if (evt === 'open' && doneCount !== 1) {
					fn();
				} else if (evt !== 'open' && doneCount === 1) {
					if (!thrownError) {
						thrownError = true;
						fn({message: 'bad'});
					} else {
						doneCount++;
					}
				}
			}

			, each: function (query, fn, done) {
				for (var i = 0; i < 4; i++) {
					fn(null, callCount * 4 + i);
				}

				done(null, 4);
				callCount++;
			}
		};
	};

	distQuery = new q(Context, '', function () {return 'a/b/c.sq3.bz2';});
	distQuery.query({
		start: '2000-01-01'
		, end: '2000-01-03'
		, type: distQuery.QueryType.EACH
		, mode: distQuery.QueryMode.ORDERED
		, outputFn: outputFn
		, onComplete: onComplete
	});

	test.done();
};

exports.testQueryError = function (test) {
	test.expect(4);

	var Context = getMockContext(this);

	Context.Cache.create = function () {
		return {
			on: function () {}
			, put: function () {}
			, get: function () {
				return;
			}
		};
	};

	Context.FileUtils.copyFile = testUtils.injectError();

	var distQuery = new q(Context, '', function () {return 'a/b/c.sq3.bz2';});
	distQuery.setQueryRange('2000-01-01', '2000-01-01');
	distQuery.run(null, {}, function () {});

	test.strictEqual(
		this.error
		, 'There was an error running the query for 01/01/2000: bad');

	Context.FileUtils.copyFile = function (src, dest, fn) {
		fn();
	};

	Context.FileUtils.unzip = function (archive, fn) {
		fn(1);
	};

	Context.Log.error = function (msg, day, errMsg) {
		test.strictEqual(
			msg
			, 'There was an error running the query for %s: %s');

		test.strictEqual(day, '01/01/2000');
		test.strictEqual(errMsg, 'Unable to extract a/b/c.sq3.bz2');
		test.done();
	};

	distQuery = new q(Context, '', function () {return 'a/b/c.sq3.bz2';});
	distQuery.setQueryRange('2000-01-01', '2000-01-01');
	distQuery.run(null, {}, function () {});
};

exports.integrationTests = {
	setUp: function (cb) {
		var self = this;
		var Context = {};

		Context.config = {
			distQueryMaxCacheSize: 2
			, distQueryCacheDirectory: 'tests/resources/cache/'
		};

		Context.cmdLine = {debug: false};

		var Locks = require('../lib/locks.js');
		var Pool = require('../lib/resource_pool.js');
		var DatabaseLoader = require('../lib/database_loader.js');

		Context.fs = require('fs');
		Context.sqlite3 = require('sqlite3');
		Context.moment = require('moment');
		Context.Cache = require('../lib/cache.js');
		Context.FileUtils = require('../lib/file_utils.js');
		Context.RefCount = require('../lib/refcount.js');
		Context.Locks = new Locks(Context);
		Context.Pool = new Pool(Context);

		DatabaseLoader = new DatabaseLoader(Context);

		Context.fs.mkdirSync('tests/resources/logs');
		Context.fs.mkdirSync('tests/resources/logs/2000');
		Context.fs.mkdirSync('tests/resources/logs/2000/01');

		self.filenameFn = function (day) {
			return fmt(
				'%s.%d.log.sq3.bz2'
				, day.format('YYYY/MM/DD')
				, day.valueOf());
		};

		var zipped = function (code, zipped) {
			if (code !== 0) {
				console.error('Error zipping ' + zipped);
			}

			numZipped++;
			if (numZipped >= 4) {
				cb();
			}
		};

		var rowsInserted = function (day, err) {
			if (err != null) {
				console.error(err);
			}

			var file = 'tests/resources/logs/' + self.filenameFn(day);
			file = file.substring(0, file.length - 4);
			Context.FileUtils.zip(file, zipped);
		};

		var dbLoaded = function (day, db) {
			var rows = [];
			var query =
				'INSERT INTO log (timestamp, channel, user, subpacket) VALUES ';

			for (var i = 0; i < 10; i++) {
				var timestamp = day.valueOf() + i * 360000;
				rows.push("(" + timestamp + ", 'chan', 'user', 'body')");
			}

			db.run(query + rows.join(', '), rowsInserted.curry(day));
		};

		var start = Context.moment('2000-01-01').utc().startOf('day');
		var numZipped = 0;
		for (var i = 0; i < 4; i++) {
			var day = Context.moment(start).add(i, 'days');
			var file = 'tests/resources/logs/' + self.filenameFn(day);
			file = file.substring(0, file.length - 4);
			DatabaseLoader.load(file, 'db/log.sql', dbLoaded.curry(day));
		}

		testUtils.interceptLogging(Context, self);
		self.Context = Context;
	},

	tearDown: function (cb) {
		var self = this;

		self.Context.FileUtils.deleteDir(
			'tests/resources/logs'
			, function (code) {
				if (code !== 0) {
					console.error('Error deleting tests/resources/logs.');
				}

				self.Context.FileUtils.deleteDir(
					'tests/resources/cache'
					, function (err) {
						if (code !== 0) {
							console.error(
								'Error deleting tests/resources/cache');
						}

						cb();
					}
				);
			}
		);
	},

	testGet: function (test) {
		test.expect(12);

		var self = this;
		var distQuery =
			new q(self.Context, 'tests/resources/logs/', self.filenameFn);

		var count = 0;
		var outputFn = function (day, err, out) {
			var calculatedDay =
				self.Context.moment('2000-01-01')
					.add(count, 'days')
					.startOf('day');

			test.deepEqual(
				out
				, {timestamp: calculatedDay.valueOf() + 9 * 360000});

			test.strictEqual(err, null);
			test.strictEqual(day.valueOf(), calculatedDay.valueOf());

			count++;
			if (count >= 4) {
				test.done();
			}
		};

		distQuery.query({
			start: '2000-01-01'
			, end: '2000-01-04'
			, type: distQuery.QueryType.ONE
			, mode: distQuery.QueryMode.ORDERED
			, outputFn: outputFn
			, query: 'SELECT timestamp FROM log ORDER BY timestamp DESC LIMIT 1'
		});
	},

	testAll: function (test) {
		test.expect(10);

		var self = this;
		var distQuery =
			new q(self.Context, 'tests/resources/logs/', self.filenameFn);

		var count = 0;
		var outputFn = function (day, err, out) {
			var calculatedDay =
				self.Context.moment('2000-01-01')
					.add((count > 0) ? count + 1 : 0, 'days')
					.startOf('day');

			test.deepEqual(
				out
				, [
					{timestamp: calculatedDay.valueOf() + 9 * 360000}
					, {timestamp: calculatedDay.valueOf() + 8 * 360000}
				]
			);

			test.strictEqual(err, null);
			test.strictEqual(day.valueOf(), calculatedDay.valueOf());

			count++;
			if (count >= 3) {
				test.strictEqual(
					self.error
					, "There was an error running the query for 02/01/2000: "
					+ "ENOENT, open 'tests/resources/logs/2000/01/02.9467712"
					+ "00000.log.sq3.bz2'");
				test.done();
			}
		};

		self.Context.fs.unlink(
			'tests/resources/logs/2000/01/02.946771200000.log.sq3.bz2');

		distQuery.query({
			start: '2000-01-01'
			, end: '2000-01-04'
			, type: distQuery.QueryType.ALL
			, mode: distQuery.QueryMode.ORDERED
			, outputFn: outputFn
			, query: 'SELECT timestamp FROM log ORDER BY timestamp DESC LIMIT 2'
		});
	},

	testEach: function (test) {
		test.expect(45);

		var self = this;
		var distQuery =
			new q(self.Context, 'tests/resources/logs/', self.filenameFn);

		var count = 0;
		var iteration = 0;

		var outputFn = function (day, err, out) {
			var calculatedDay =
				self.Context.moment('2000-01-01')
					.add((count === 2) ? 3 : count, 'days')
					.startOf('day');

			var timestamp = calculatedDay.valueOf() + (9 - iteration) * 360000;

			test.deepEqual(out, {timestamp: timestamp});
			test.strictEqual(err, null);
			test.strictEqual(day.valueOf(), calculatedDay.valueOf());

			iteration++;
		};

		var onComplete = function (day, err, numRows) {
			test.strictEqual(
				day.valueOf()
				, self.Context.moment('2000-01-01')
					.add((count === 2) ? 3 : count, 'days')
					.startOf('day')
					.valueOf());

			test.strictEqual(err, null);
			test.strictEqual(numRows, 4);

			iteration = 0;
			count++;
			if (count >= 3) {
				test.done();
			}
		};

		self.Context.fs.unlink(
			'tests/resources/logs/2000/01/03.946857600000.log.sq3.bz2');

		distQuery.query({
			start: '2000-01-01'
			, end: '2000-01-04'
			, type: distQuery.QueryType.EACH
			, mode: distQuery.QueryMode.ORDERED
			, outputFn: outputFn
			, onComplete: onComplete
			, query: 'SELECT timestamp FROM log ORDER BY timestamp DESC LIMIT 4'
		});
	}
};
