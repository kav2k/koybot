'use strict';

var moment = require('moment');
var fmt = require('util').format;
var fs = require('fs');
var zlib = require('zlib');
var lg = require('../lib/file_utils.js');

exports.testCopyFile = function (test) {
	test.expect(3);
	fs.writeFileSync('tests/resources/tmp', '1');

	lg.copyFile('tests/resources/404', 'tests/resources/copy', function (err) {
		test.notEqual(err, null);

		lg.copyFile('tests/resources/tmp', 'tests/resources/copy', function (err) {
			test.equal(err, null);
			test.notEqual(fs.statSync('tests/resources/copy'), null);
			fs.unlinkSync('tests/resources/copy');
			fs.unlinkSync('tests/resources/tmp');

			test.done();
		});
	});
};

exports.testDeleteDir = function (test) {
	test.expect(2);

	fs.mkdirSync('tests/resources/tmp');
	lg.deleteDir('tests/resources/tmp', function (code) {
		test.strictEqual(code, 0);

		try {
			fs.statSync('tests/resources/tmp');
		} catch (e) {
			test.notEqual(e, null);
		}

		test.done();
	});
};

exports.testUnzip = function (test) {
	test.expect(3);

	zlib.gzip(new Buffer('1', 'utf8'), function (err, buf) {
		fs.writeFileSync('tests/resources/tmp.bz2', buf);
		test.strictEqual(err, null);
		lg.unzip('tests/resources/tmp.bz2', function (code, unzipped) {
			var contents = fs.readFileSync(unzipped, {encoding: 'utf8'});
			test.strictEqual(code, 0);
			test.strictEqual(contents, '1');

			fs.unlinkSync(unzipped);
			test.done();
		});
	});
};

exports.testZip = function (test) {
	test.expect(3);

	fs.writeFileSync(
		'tests/resources/tmp'
		, '11111111111111111111111111111111'
		+ '11111111111111111111111111111111'
		+ '11111111111111111111111111111111'
		+ '11111111111111111111111111111111');

	var stats = fs.statSync('tests/resources/tmp');
	var sizeBefore = stats.size;

	lg.zip('tests/resources/tmp', function (code, filename) {
		test.strictEqual(code, 0);
		test.strictEqual(filename, 'tests/resources/tmp.bz2');

		var stats = fs.statSync(filename);
		test.ok(stats.size < sizeBefore);

		fs.unlinkSync(filename);
		test.done();
	});
};
