'use strict';

var fmt = require('util').format;
var ListenerLoader = require('../lib/listener_loader.js');
var utils = require('../lib/test_utils.js');

exports.tests = {
	setUp: function (cb) {
		var self = this;
		self.Context = {
			maindb: true
			, logdb: true
			, fmt: fmt
		};
		utils.interceptLogging(self.Context, self);
		self.error = null;
		cb();
	},

	testNoDBs: function (test) {
		test.expect(1);

		delete this.Context.maindb;
		delete this.Context.logdb;
		var ll = new ListenerLoader(this.Context);
		ll.load();
		test.strictEqual(
			this.error
			, 'Databases unavailable, loading listeners aborted.');

		test.done();
	},

	testBadDirectory: function (test) {
		test.expect(1);

		this.Context.fs = {
			readdirSync: function () {
				throw new Error('');
			}
		};

		var ll = new ListenerLoader(this.Context);
		ll.load();
		test.strictEqual(this.error, 'Unable to load listeners: ');

		test.done();
	},

	testBadFile: function (test) {
		test.expect(1);

		this.Context.fs = {readdirSync: function () {return ['404'];}};

		var ll = new ListenerLoader(this.Context);
		ll.load();
		test.strictEqual(this.error, 'Could not initialise listener 404');

		test.done();
	},

	testIgnoresDisabled: function (test) {
		test.expect(1);

		this.Context.fs = {readdirSync: function () {return ['disabled'];}};

		var ll = new ListenerLoader(this.Context);
		ll.load();
		test.strictEqual(this.error, null);

		test.done();
	},

	testPassesCorrectArgs: function (test) {
		test.expect(3);

		this.Context.fs = require('fs');
		this.Context.init = false;

		var b = {init: false};
		var c = {init: false};

		var ll = new ListenerLoader(this.Context, b, c);
		ll.load('tests/resources/listeners');

		test.ok(this.Context.init);
		test.ok(b.init);
		test.ok(c.init);

		test.done();
	},

	testClearsModuleCache: function (test) {
		test.expect(1);

		require.cache.test = true;
		var ll = new ListenerLoader(this.Context);
		ll.load();
		test.strictEqual(Object.getOwnPropertyNames(require.cache).length, 0);

		test.done();
	}
};
