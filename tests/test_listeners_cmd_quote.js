'use strict';

var quote = require('../listeners/cmd_quote.js');
var testUtils = require('../lib/test_utils.js');
var dbUtils = require('../lib/db_utils.js');

var getMockContext = function (attach) {
	var Packet = require('../lib/packet.js');
	var fmt = require('util').format;
	var Context = {};

	Context.fmt = fmt;
	Context.Packet = new Packet(Context);
	Context.config = {cmdChar: ['!'], name: 'bot'};
	Context.maindb = {};
	Context.HelpText = {};
	Context.logdb = {
		all: function (query, values, fn) {
			fn(null, [{subpacket: 'msg main\nfrom=user\n\nb'}]);
		}
	};

	testUtils.interceptLogging(Context, attach);
	return Context;
};

var getMockKeyValueStore = function () {
	return {put: function () {}};
};

exports.testConfigOverride = function (test) {
	test.expect(2);

	var Context = getMockContext();
	var EventProcessor = testUtils.getMockEventProcessor({content: 'a b'});

	Context.logdb.all = function (query, values) {
		test.strictEqual(values.$limit, 100);
	};

	var q = new quote(Context, EventProcessor);

	Context.config = {quoteLookback: 1};
	Context.logdb.all = function (query, values) {
		test.strictEqual(values.$limit, 1);
	};

	q = new quote(Context, EventProcessor);

	test.done();
};

exports.testSearchByUser = function (test) {
	test.expect();

	var Context = getMockContext();
	var EventProcessor = testUtils.getMockEventProcessor(
		{content: '!quote &lt;user&gt; find me'});

	Context.logdb.all = function (query, values) {
		test.strictEqual(values.$user, 'user');
		test.ok(query.indexOf('AND user = $user ') > -1);
	};

	var q = new quote(Context, EventProcessor);

	EventProcessor = testUtils.getMockEventProcessor({content: '!quote'});

	Context.logdb.all = function (query, values) {
		test.strictEqual(values.$user, 'bot');
		test.ok(query.indexOf('AND user <> $user ') > -1);
	};

	q = new quote(Context, EventProcessor);

	test.done();
};

exports.testErrorRetrievingRecentRows = function (test) {
	test.expect(1);

	var Context = getMockContext(this);
	var EventProcessor = testUtils.getMockEventProcessor({content: 'a b'});
	Context.logdb.all = testUtils.injectError();

	var q = new quote(Context, EventProcessor);
	test.strictEqual(
		this.error
		, 'Error retrieving recent log rows to search for quote: bad');

	test.done();
};

exports.testFindsMostRecentQuote = function (test) {
	test.expect(1);

	var Context = getMockContext(this);
	var EventProcessor =
		testUtils.getMockEventProcessor({content: '!quote find me'});

	Context.logdb.all = function (query, values, fn) {
		fn(null, [
			{}
			, {subpacket: '\n'}
			, {subpacket: 'msg main\n='}
			, {subpacket: 'msg main'}
			, {subpacket: 'msg main\n\ncan you find me?'}
			, {subpacket: 'msg main\n\ndon\'t find me'}
		]);
	};

	Context.maindb.run = function (query, values) {
		test.strictEqual(values.$quote, 'can you find me?');
	};

	var q = new quote(Context, EventProcessor);

	test.done();
};

exports.testExtractCorrectQuoted = function (test) {
	test.expect(2);

	var Context = getMockContext(this);
	var EventProcessor = testUtils.getMockEventProcessor({content: 'a b'});

	Context.logdb.all = function (query, values, fn) {
		fn(null, [{subpacket: 'msg main\nfrom=from\n\nb'}]);
	};

	Context.maindb.run = function (query, values) {
		test.strictEqual(values.$quoted, 'from');
	};

	var q = new quote(Context, EventProcessor);

	Context.logdb.all = function (query, values, fn) {
		fn(null, [{subpacket: 'join user\n\nb'}]);
	};

	Context.maindb.run = function (query, values) {
		test.strictEqual(values.$quoted, 'user');
	};

	q = new quote(Context, EventProcessor);

	test.done();
};

exports.testErrorStoringQuote = function (test) {
	test.expect(1);

	var Context = getMockContext(this);
	var EventProcessor = testUtils.getMockEventProcessor({content: 'a b'});
	Context.maindb.run = testUtils.injectError();

	var q = new quote(Context, EventProcessor);
	test.strictEqual(this.error, 'Error attempting to store quote: bad');

	test.done();
};

exports.testNoLastID = function (test) {
	test.expect(1);

	var Context = getMockContext(this);
	var EventProcessor = testUtils.getMockEventProcessor({content: 'a b'});

	Context.maindb.run = function (query, values, fn) {
		fn.call({}, null);
	};

	var q = new quote(Context, EventProcessor);
	test.strictEqual(
		this.error
		, 'Unable to retrieve last inserted '
		+ 'row ID when storing quote.');

	test.done();
};

exports.testSendMsg = function (test) {
	test.expect(5);

	var Context = getMockContext(this);
	var EventProcessor =
		testUtils.getMockEventProcessor({content: 'a b', channel: 'chan'});

	var dAmn = {};
	Context.memory = {};
	Context.KeyValueStore = {};

	Context.maindb.run = function (query, values, fn) {
		fn.call({lastID: 42}, null);
	};

	Context.KeyValueStore.put = function (key, value, cb) {
		test.strictEqual(key, 'RecentQuoteID');
		test.strictEqual(value, 42);
	};

	dAmn.sendMsg = function (channel, msg) {
		test.strictEqual(channel, 'chan');
		test.strictEqual(msg, 'Added quote #42.');
	};

	var q = new quote(Context, EventProcessor, dAmn);
	test.strictEqual(Context.memory.recentQuoteID, 42);

	test.done();
};

exports.testCaseInsensitiveSearch = function (test) {
	test.expect(1);

	var Context = getMockContext(this);
	var EventProcessor =
		testUtils.getMockEventProcessor({content: '!quote find me'});

	Context.logdb.all = function (query, values, fn) {
		fn(null, [
			{subpacket: 'msg main\n\ncan you FIND ME?'}
			, {subpacket: 'msg main\n\ndon\'t find me'}
		]);
	};

	Context.maindb.run = function (query, values) {
		test.strictEqual(values.$quote, 'can you FIND ME?');
	};

	var q = new quote(Context, EventProcessor);

	test.done();
};

exports.testIgnoresBotCommands = function (test) {
	test.expect(1);

	var Context = getMockContext(this);
	var EventProcessor =
		testUtils.getMockEventProcessor({content: '!quote find me'});

	Context.logdb.all = function (query, values, fn) {
		fn(null, [
			{subpacket: 'msg main\n\n!quote find me'}
			, {subpacket: 'msg main\n\ncan you find me?'}
		]);
	};

	Context.maindb.run = function (query, values) {
		test.strictEqual(values.$quote, 'can you find me?');
	};

	var q = new quote(Context, EventProcessor);

	test.done();
};

exports.testQuoteLastLine = function (test) {
	test.expect(2);

	var Context = getMockContext(this);
	var EventProcessor = testUtils.getMockEventProcessor({content: '!quote'});

	Context.logdb.all = function (query, values, fn) {
		fn(null, [
			{subpacket: 'msg main\n\n!quote'}
			, {subpacket: 'msg main\n\nquote this'}
			, {subpacket: 'msg main\n\ndo not quote this'}
		]);
	};

	Context.maindb.run = function (query, values) {
		test.strictEqual(values.$quote, 'quote this');
	};

	var q = new quote(Context, EventProcessor);

	EventProcessor = testUtils.getMockEventProcessor({content: '!quote '});
	Context.maindb.run = function (query, values) {
		test.strictEqual(values.$quote, 'quote this');
	};

	q = new quote(Context, EventProcessor);

	test.done();
};

exports.integrationTests = {
	setUp: function (cb) {
		var self = this;
		var moment = require('moment');
		var Log = require('../lib/log.js');
		var Packet = require('../lib/packet.js');

		self.Context = {};
		self.Context.config = {cmdChar: ['!'], name: 'bot'};
		self.Context.fmt = require('util').format;
		self.Context.memory = {};
		self.Context.fs = require('fs');
		self.Context.sqlite3 = require('sqlite3');
		self.Context.HelpText = {};
		self.Context.Log = new Log(false);
		self.Context.Packet = new Packet(self.Context);
		self.Context.KeyValueStore = getMockKeyValueStore();

		var now = moment().utc().valueOf();
		dbUtils.setupDBs(
			self.Context
			, cb
			, {logdb: [{
					tableName: 'log'
					, cols: ['user', 'subpacket', 'timestamp', 'channel']
				} , ['user', 'msg main\nfrom=user\n\nquote me', now, 'chan']
				, ['bot', 'msg main\nfrom=bot\n\nI am a bot quote'
					, now + 1, 'chan']
				, ['user', 'msg main\nfrom=user\n\nignore me', now + 2, 'chan']
			]}
		);
	},

	tearDown: function (cb) {
		dbUtils.cleanupDBs(this.Context, cb);
	},

	testQuoteNotSelf: function (test) {
		test.expect(7);
		var self = this;

		var dAmn = {};
		var EventProcessor = testUtils.getMockEventProcessor(
			{from: 'user', content: '!quote quote', channel: 'chan'});

		dAmn.sendMsg = function (channel, msg) {
			self.Context.maindb.get(
				'SELECT * FROM quotes'
				, function (err, row) {
					test.strictEqual(err, null);
					test.strictEqual(row.id, 1);
					test.strictEqual(row.channel, 'chan');
					test.strictEqual(row.quoter, 'user');
					test.strictEqual(row.quoted, 'user');
					test.strictEqual(row.quote, 'quote me');
					test.strictEqual(row.msg_type, 'msg');
					test.done();
				}
			);
		};

		new quote(self.Context, EventProcessor, dAmn);
	},

	testQuoteUser: function (test) {
		test.expect(7);
		var self = this;

		var dAmn = {};
		var EventProcessor = testUtils.getMockEventProcessor({
			from: 'user'
			, content: '!quote &lt;bot&gt; quote'
			, channel: 'chan'
		});

		dAmn.sendMsg = function (channel, msg) {
			self.Context.maindb.get(
				'SELECT * FROM quotes'
				, function (err, row) {
					test.strictEqual(err, null);
					test.strictEqual(row.id, 1);
					test.strictEqual(row.channel, 'chan');
					test.strictEqual(row.quoter, 'user');
					test.strictEqual(row.quoted, 'bot');
					test.strictEqual(row.quote, 'I am a bot quote');
					test.strictEqual(row.msg_type, 'msg');
					test.done();
				}
			);
		};

		new quote(self.Context, EventProcessor, dAmn);
	}
};

