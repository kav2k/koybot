'use strict';

var randomquote = require('../listeners/cmd_randomquote.js');
var utils = require('../lib/test_utils.js');

exports.testNoUser = function (test) {
	test.expect(4);

	var Context = utils.getMockContext(this);
	var EventProcessor = utils.getMockEventProcessor({content: '!randomquote'});

	Context.maindb.get = function (query, values) {
		test.equal(values.$quoted, null);
		test.ok(query.indexOf('WHERE') < 0);
	};

	var q = new randomquote(Context, EventProcessor);

	EventProcessor = utils.getMockEventProcessor({content: '!randomquote '});
	q = new randomquote(Context, EventProcessor);

	test.done();
};

exports.testUser = function (test) {
	test.expect(4);

	var Context = utils.getMockContext(this);
	var EventProcessor =
		utils.getMockEventProcessor({content: '!randomquote user'});

	Context.maindb.get = function (query, values) {
		test.strictEqual(values.$quoted, 'user');
		test.ok(query.indexOf('WHERE') > -1);
	};

	var q = new randomquote(Context, EventProcessor);

	EventProcessor =
		utils.getMockEventProcessor({content: '!randomquote &lt;user&gt; '});
	q = new randomquote(Context, EventProcessor);

	test.done();
};

exports.testCountQuotesFail = function (test) {
	test.expect(1);

	var Context = utils.getMockContext(this);
	var EventProcessor = utils.getMockEventProcessor({content: '!randomquote'});
	Context.maindb.get = utils.injectError();

	var q = new randomquote(Context, EventProcessor);
	test.strictEqual(this.error, 'Unable to determine number of quotes: bad');

	test.done();
};

exports.testNoQuotes = function (test) {
	test.expect(0);

	var Context = utils.getMockContext(this);
	var EventProcessor = utils.getMockEventProcessor({content: '!randomquote'});
	var dAmn = {};

	Context.maindb.get = function (query, values, cb) {
		cb(null, undefined);
	};

	dAmn.sendMsg = function () {
		test.ok(false);
	};

	var q = randomquote(Context, EventProcessor, dAmn);

	test.done();
};

exports.testRandomQuoteNoUser = function (test) {
	test.expect(2);

	var Context = utils.getMockContext(this);
	var EventProcessor = utils.getMockEventProcessor({content: '!randomquote'});

	var getEntries = 0;
	Context.maindb.get = function (query, values, cb) {
		getEntries++;

		if (getEntries === 1) {
			cb(null, {'count(*)': 1});
		} else {
			test.equal(values.$quoted, null);
			test.ok(query.indexOf('WHICH') < 0);
		}
	};

	var q = new randomquote(Context, EventProcessor);

	test.done();
};

exports.testRandomQuoteUser = function (test) {
	test.expect(2);

	var Context = utils.getMockContext(this);
	var EventProcessor =
		utils.getMockEventProcessor({content: '!randomquote user'});

	var getEntries = 0;
	Context.maindb.get = function (query, values, cb) {
		getEntries++;

		if (getEntries === 1) {
			cb(null, {'count(*)': 1});
		} else {
			test.strictEqual(values.$quoted, 'user');
			test.ok(query.indexOf('WHERE') > -1);
		}
	};

	var q = new randomquote(Context, EventProcessor);

	test.done();
};

exports.testOffset = function (test) {
	test.expect(1);

	var Context = utils.getMockContext(this);
	var EventProcessor = utils.getMockEventProcessor({content: '!randomquote'});

	var getEntries = 0;
	Context.maindb.get = function (query, values, cb) {
		getEntries++;

		if (getEntries === 1) {
			cb(null, {'count(*)': 10});
		} else {
			test.ok(values.$offset >= 0 && values.$offset < 10);
		}
	};

	var q = new randomquote(Context, EventProcessor);

	test.done();
};

exports.testRandomQuoteError = function (test) {
	test.expect(1);

	var Context = utils.getMockContext(this);
	var EventProcessor = utils.getMockEventProcessor({content: '!randomquote'});

	var getEntries = 0;
	Context.maindb.get = function (query, values, cb) {
		getEntries++;

		if (getEntries === 1) {
			cb(null, {'count(*)': 1});
		} else {
			cb({message: 'bad'});
		}
	};

	var q = new randomquote(Context, EventProcessor);
	test.strictEqual(this.error, 'Unable to retrieve quote at offset 0: bad');

	test.done();
};

exports.testSuccess = function (test) {
	test.expect(1);

	var Context = utils.getMockContext(this);
	var EventProcessor = utils.getMockEventProcessor({content: '!randomquote'});
	var dAmn = {};

	var getEntries = 0;
	Context.maindb.get = function (query, values, cb) {
		getEntries++;

		if (getEntries === 1) {
			cb(null, {'count(*)': 1});
		} else {
			cb(null, {id: 666, quoted: 'user', quote: 'out of context'});
		}
	};

	dAmn.sendMsg = function (channel, msg) {
		test.strictEqual(msg, '#666 &lt;user&gt; out of context');
	};

	var q = new randomquote(Context, EventProcessor, dAmn);

	test.done();
};

exports.testTablumpsConversion = function (test) {
	test.expect(1);

	var Context = utils.getMockContext(this);
	var EventProcessor = utils.getMockEventProcessor({content: '!randomquote'});
	var dAmn = {};

	var getEntries = 0;
	Context.maindb.get = function (query, values, cb) {
		getEntries++;

		if (getEntries === 1) {
			cb(null, {'count(*)': 1});
		} else {
			cb(null, {
				id: 666
				, quoted: 'user'
				, quote: 'I am &emote\t:(\t15\t15\t:(\t???\t happy!'
			});
		}
	};

	dAmn.sendMsg = function (channel, msg) {
		test.strictEqual(msg, '#666 &lt;user&gt; I am :( happy!');
	};

	new randomquote(Context, EventProcessor, dAmn);
	test.done();
};

