'use strict';

require('../lib/function.js');
var reload = require('../listeners/cmd_reload.js');
var utils = require('../lib/test_utils.js');

exports.testIsAdminError = function (test) {
	test.expect(1);

	var Context = utils.getMockContext(this);
	var EventProcessor = utils.getMockEventProcessor({
		content: '!reload'
		, from: 'user'
	});

	Context.Admin.isAdmin = utils.getMockIsAdmin({message: 'bad'});

	var rl = new reload(Context, EventProcessor);

	test.strictEqual(
		this.error
		, 'Unable to check if "user" has admin privileges '
		+ 'to run reload command: bad');

	test.done();
};

exports.testAccessDenied = function (test) {
	test.expect(2);

	var Context = utils.getMockContext(this);
	var EventProcessor = utils.getMockEventProcessor({
		content: '!reload'
		, channel: 'chan'
	});

	Context.Admin.isAdmin = utils.getMockIsAdmin(null, false);

	var dAmn = {};
	dAmn.sendMsg = function (channel, msg) {
		test.strictEqual(channel, 'chan');
		test.strictEqual(msg, 'Access denied.');
	};

	var rl = new reload(Context, EventProcessor, dAmn);

	test.done();
};

exports.testDoReload = function (test) {
	test.expect(4);

	var Context = utils.getMockContext(this);
	var EventProcessor = utils.getMockEventProcessor({
		content: '!reload'
		, channel: 'chan'
	});

	EventProcessor.clear = function () {
		test.ok(true);
	};

	Context.fs = {};
	Context.logdb = {};

	Context.fs.readdirSync = function (dir) {
		throw new Error('bad');
	};

	Context.Admin.isAdmin = utils.getMockIsAdmin(null, true);

	var dAmn = {};
	dAmn.sendMsg = function (channel, msg) {
		test.strictEqual(channel, 'chan');
		test.strictEqual(msg, 'Reloading listeners.');
	};

	var rl = new reload(Context, EventProcessor, dAmn);
	test.strictEqual(this.error, 'Unable to load listeners: bad');

	test.done();
};
