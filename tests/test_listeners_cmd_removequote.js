'use strict';

require('../lib/function.js');
var removequote = require('../listeners/cmd_removequote.js');
var utils = require('../lib/test_utils.js');

exports.testGetQuoteID = function (test) {
	test.expect(2);

	var Context = utils.getMockContext(this);
	Context.maindb.run = function (query, values) {
		test.strictEqual(values.$id, '20');
	};

	var inputs = [
		['!removequote', false]
		, ['!removequote ', false]
		, ['!removequote text', false]
		, ['!removequote 20', true]
		, ['!removequote #20', true]
	];

	var isAdmin = function (success) {
		return function (db, admin, fn) {
			if (success) {
				fn(null, true);
			} else {
				test.ok(false);
			}
		};
	};

	for (var i = 0; i < inputs.length; i++) {
		var input = inputs[i];
		var content = input[0];
		var success = input[1];

		var EventProcessor = utils.getMockEventProcessor({content: content});
		Context.Admin.isAdmin = isAdmin(success);
		var rm = new removequote(Context, EventProcessor);
	}

	test.done();
};

exports.testIsAdminError = function (test) {
	test.expect(1);

	var Context = utils.getMockContext(this);
	var EventProcessor = utils.getMockEventProcessor(
		{content: '!removequote #20'});

	Context.Admin.isAdmin = utils.getMockIsAdmin({message: 'bad'});

	var rm = new removequote(Context, EventProcessor);
	test.strictEqual(
		this.error
		, 'Error checking admin status when removing quote #20: bad');

	test.done();
};

exports.testCheckQuoterError = function (test) {
	test.expect(2);

	var Context = utils.getMockContext(this);
	var EventProcessor = utils.getMockEventProcessor(
		{content: '!removequote #20'});

	Context.Admin.isAdmin = utils.getMockIsAdmin(null, false);
	Context.maindb.get = utils.injectError();

	new removequote(Context, EventProcessor);
	test.strictEqual(
		 this.error
		 , 'Error checking quoter when removing quote #20: bad');

	Context.maindb.get = function (query, values, fn) {
		fn(null, {});
	};

	new removequote(Context, EventProcessor);
	test.strictEqual(
		this.error
		, 'Invalid row returned when checking quoter for quote #20: {}');

	test.done();
};

exports.testAccessDenied = function (test) {
	test.expect(2);

	var Context = utils.getMockContext(this);
	var EventProcessor = utils.getMockEventProcessor({
		content: '!removequote #20'
		, channel: 'chan'
	});

	Context.Admin.isAdmin = utils.getMockIsAdmin(null, false);
	Context.maindb.get = function (query, values, fn) {
		fn(null, {'count(*)': 0});
	};

	var dAmn = {};
	dAmn.sendMsg = function (channel, msg) {
		test.strictEqual(channel, 'chan');
		test.strictEqual(msg, 'Access denied.');
	};

	var rm = new removequote(Context, EventProcessor, dAmn);

	test.done();
};

exports.testRemoveQuoteError = function (test) {
	test.expect(1);

	var Context = utils.getMockContext(this);
	var EventProcessor = utils.getMockEventProcessor(
		{content: '!removequote #20'});

	Context.Admin.isAdmin = utils.getMockIsAdmin(null, true);
	Context.maindb.run = utils.injectError();

	var rm = new removequote(Context, EventProcessor);
	test.strictEqual(this.error, 'Error removing quote #20: bad');

	test.done();
};

exports.testRemoveQuoteIsAdminSuccess = function (test) {
	test.expect(2);

	var Context = utils.getMockContext(this);
	var EventProcessor = utils.getMockEventProcessor({
		content: '!removequote #20'
		, channel: 'chan'
	});

	Context.Admin.isAdmin = utils.getMockIsAdmin(null, true);
	Context.maindb.run = function (query, values, fn) {
		fn(null);
	};

	var dAmn = {};
	dAmn.sendMsg = function (channel, msg) {
		test.strictEqual(channel, 'chan');
		test.strictEqual(msg, 'Quote #20 removed.');
	};

	var rm = new removequote(Context, EventProcessor, dAmn);

	test.done();
};

exports.testRemoveQuoteIsQuoterSuccess = function (test) {
	test.expect(2);

	var Context = utils.getMockContext(this);
	var EventProcessor = utils.getMockEventProcessor({
		content: '!removequote #69'
		, channel: 'chan'
	});

	Context.Admin.isAdmin = utils.getMockIsAdmin(null, false);

	Context.maindb.get = function (query, values, fn) {
		fn(null, {'count(*)': 1});
	};

	Context.maindb.run = function (query, values, fn) {
		fn(null);
	};

	var dAmn = {};
	dAmn.sendMsg = function (channel, msg) {
		test.strictEqual(channel, 'chan');
		test.strictEqual(msg, 'Quote #69 removed.');
	};

	new removequote(Context, EventProcessor, dAmn);

	test.done();
};
