'use strict';

var reply = require('../listeners/cmd_reply.js');
var utils = require('../lib/test_utils.js');

exports.testReplyInvalid = function (test) {
	test.expect(0);

	var inputs = ['!reply', '!reply '];
	var Context = utils.getMockContext(this);
	Context.config = {name: ''};

	var dAmn = {};
	dAmn.sendMsg = function (channel, msg) {
		test.ok(false);
	};

	inputs.forEach(function (input) {
		var EventProcessor = utils.getMockEventProcessor({content: input});
		new reply(Context, EventProcessor, dAmn);
	});

	test.done();
};

exports.testReplySuccess = function (test) {
	test.expect(1);

	var dAmn = {};
	var Context =  utils.getMockContext(this);
	var EventProcessor =
		utils.getMockEventProcessor({content: '!reply to this', from: 'User'});

	Context.config = {name: ''};
	dAmn.sendMsg = function (channel, msg) {
		test.strictEqual(msg, 'User: to this');
	};

	new reply(Context, EventProcessor, dAmn);
	test.done();
};
