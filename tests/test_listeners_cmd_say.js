'use strict';

var say = require('../listeners/cmd_say.js');
var utils = require('../lib/test_utils.js');

exports.testSayInvalid = function (test) {
	test.expect(0);

	var inputs = ['!say', '!say '];
	var Context = utils.getMockContext(this);
	Context.config = {name: ''};

	var dAmn = {};
	dAmn.sendMsg = function (channel, msg) {
		test.ok(false);
	};

	inputs.forEach(function (input) {
		var EventProcessor = utils.getMockEventProcessor({content: input});
		new say(Context, EventProcessor, dAmn);
	});

	test.done();
};

exports.testSaySuccess = function (test) {
	test.expect(1);

	var dAmn = {};
	var Context = utils.getMockContext(this);
	var EventProcessor = utils.getMockEventProcessor({content: '!say this'});

	Context.config = {name: ''};
	dAmn.sendMsg = function (channel, msg) {
		test.strictEqual(msg, 'this');
	};

	new say(Context, EventProcessor, dAmn);
	test.done();
};
