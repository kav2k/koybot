'use strict';

require('../lib/function.js');
var listeners = require('../listeners/listeners.js');
var testUtils = require('../lib/test_utils.js');

exports.testInvalid = function (test) {
	test.expect(0);

	var Context = testUtils.getMockContext(this);
	var EventProcessor =
		testUtils.getMockEventProcessor('cmd_disable', {content: '!disable'});

	Context.Admin.isAdmin = function (db, from, fn) {
		test.ok(false);
	};

	new listeners(Context, EventProcessor);

	EventProcessor =
		testUtils.getMockEventProcessor('cmd_enable', {content: '!enable'});

	new listeners(Context, EventProcessor);

	test.done();
};

exports.testCheckAdmin = function (test) {
	test.expect(1);

	var Context = testUtils.getMockContext(this);
	var EventProcessor = testUtils.getMockEventProcessor(
		'cmd_disable'
		, {content: '!diable mod', from: 'user'});

	Context.Admin.isAdmin = testUtils.getMockIsAdmin({message: 'bad'});
	new listeners(Context, EventProcessor);

	test.strictEqual(
		this.error
		, 'Unable to check if "user" has admin privileges '
		+ 'to run enable/disable command: bad');

	test.done();
};

exports.testAccessDenied = function (test) {
	test.expect(1);

	var dAmn = {};
	var Context = testUtils.getMockContext(this);
	var EventProcessor = testUtils.getMockEventProcessor(
		'cmd_enable'
		, {content: '!enable mod', from: 'user'});

	Context.Admin.isAdmin = testUtils.getMockIsAdmin(null, false);
	dAmn.sendMsg = function (channel, msg) {
		test.strictEqual(msg, 'Access denied.');
	};

	new listeners(Context, EventProcessor, dAmn);
	test.done();
};

exports.testNotFound = function (test) {
	test.expect(2);

	var dAmn = {};
	var Context = testUtils.getMockContext(this);
	var EventProcessor = testUtils.getMockEventProcessor(
		'cmd_disable'
		, {content: '!disable mod', from: 'user'});

	Context.Admin.isAdmin = testUtils.getMockIsAdmin(null, true);
	Context.fs = {stat: testUtils.injectError()};

	dAmn.sendMsg = function (channel, msg) {
		test.strictEqual(msg, 'Module "mod" not found or already disabled.');
	};

	new listeners(Context, EventProcessor, dAmn);

	EventProcessor = testUtils.getMockEventProcessor(
		'cmd_enable', {content: '!enable mod', from: 'user'});

	dAmn.sendMsg = function (channel, msg) {
		test.strictEqual(msg, 'Module "mod" not found or already enabled.');
	};

	new listeners(Context, EventProcessor, dAmn);

	test.done();
};

exports.testMoveModuleIOError = function (test) {
	test.expect(4);

	var self = this;
	var Context = testUtils.getMockContext(self);
	var EventProcessor = testUtils.getMockEventProcessor(
		'cmd_enable'
		, {content: '!enable mod', from: 'user'});

	var enabledTest = function () {
		test.strictEqual(
			self.error
			, 'Error moving "listeners/disabled/mod.js" '
			+ 'to "listeners/mod.js": bad');
	};

	var disabledTest = function () {
		test.strictEqual(
			self.error
			, 'Error moving "listeners/mod.js" '
			+ 'to "listeners/disabled/mod.js": bad');
	};

	Context.Admin.isAdmin = testUtils.getMockIsAdmin(null, true);

	Context.fs = {};
	Context.fs.stat = function (path, fn) {
		fn(null, {});
	};

	Context.FileUtils = {};
	Context.FileUtils.copyFile = testUtils.injectError();

	new listeners(Context, EventProcessor);
	enabledTest();

	Context.FileUtils.copyFile = function (src, dest, fn) {
		fn(null);
	};

	Context.fs.unlink = testUtils.injectError();

	new listeners(Context, EventProcessor);
	enabledTest();

	EventProcessor = testUtils.getMockEventProcessor(
		'cmd_disable'
		, {content: '!diable mod', from: 'user'});

	Context.FileUtils.copyFile = testUtils.injectError();

	new listeners(Context, EventProcessor);
	disabledTest();

	Context.FileUtils.copyFile = function (src, dest, fn) {
		fn(null);
	};

	new listeners(Context, EventProcessor);
	disabledTest();

	test.done();
};

exports.testReloadOnSuccess = function (test) {
	test.expect(8);

	var dAmn = {};
	var Context = testUtils.getMockContext(this);
	var EventProcessor = testUtils.getMockEventProcessor(
		'cmd_enable'
		, {content: '1enable mod', from: 'user'});

	Context.Admin.isAdmin = testUtils.getMockIsAdmin(null, true);
	Context.fs = {};
	Context.FileUtils = {};
	Context.config = {cmdChar: ['!']};

	Context.fs.stat = function (path, fn) {
		fn(null, {});
	};

	Context.fs.unlink = function (path, fn) {
		fn(null);
	};

	Context.FileUtils.copyFile = function (src, dest, fn) {
		fn(null);
	};

	EventProcessor.fire = function (evt, recv) {
		test.strictEqual(evt, 'cmd_reload');
		test.strictEqual(recv.content, '!reload');
		test.strictEqual(recv.from, 'user');
	};

	dAmn.sendMsg = function (channel, msg) {
		test.strictEqual(msg, 'Module "mod" enabled.');
	};

	new listeners(Context, EventProcessor, dAmn);

	EventProcessor.register = function (evt, fn) {
		if (evt === 'cmd_enable') {
			return;
		}

		fn({content: '!disable mod', from: 'user'});
	};

	dAmn.sendMsg = function (channel, msg) {
		test.strictEqual(msg, 'Module "mod" disabled.');
	};

	new listeners(Context, EventProcessor, dAmn);

	test.done();
};

exports.integrationTests = {
	setUp: function (cb) {
		var self = this;
		var Log = require('../lib/log.js');
		var Packet = require('../lib/packet.js');

		var Context = {};
		Context.HelpText = {};
		Context.Admin = {};
		Context.fs = require('fs');
		Context.fmt = require('util').format;
		Context.Log = new Log(false);
		Context.Packet = new Packet(Context);
		Context.FileUtils = require('../lib/file_utils.js');

		Context.config = {
			cmdChar: ['!']
			, listenerDirectory: 'tests/resources/listeners'
		};

		Context.Admin.isAdmin = testUtils.getMockIsAdmin(null, true);

		Context.fs.mkdir('tests/resources/listeners/disabled', function (err) {
			if (err) {
				throw err;
			}

			Context.fs.writeFile(
				'tests/resources/listeners/mod.js'
				, ''
				, function (err) {
					if (err) {
						throw err;
					}

					self.Context = Context;
					cb();
				}
			);
		});
	},

	tearDown: function (cb) {
		var self = this;

		self.Context.fs.rmdir(
			'tests/resources/listeners/disabled'
			, function (err) {
				if (err) {
					throw err;
				}

				self.Context.fs.unlink(
					'tests/resources/listeners/mod.js'
					, function (err) {
						if (err) {
							throw err;
						}

						cb();
					}
				);
			}
		);
	},

	testDisableEnable: function (test) {
		test.expect(4);

		var self = this;
		var dAmn = {};
		var EventProcessor = testUtils.getMockEventProcessor(
			'cmd_disable'
			, {content: '!disable mod', from: 'user'});

		EventProcessor.fire = function (evt, recv) {};

		dAmn.sendMsg = function (channel, msg) {
			self.Context.fs.stat(
				'tests/resources/listeners/disabled/mod.js'
				, function (err, stat) {
					test.strictEqual(err, null);

					self.Context.fs.stat(
						'tests/resources/listeners/mod.js'
						, function (err, stat) {
							test.strictEqual(err.code, 'ENOENT');
							testEnable();
						}
					);
				}
			);
		};

		new listeners(self.Context, EventProcessor, dAmn);

		var testEnable = function () {
			EventProcessor.register = function (evt, fn) {
				if (evt === 'cmd_disable') {
					return;
				}

				fn({content: '!enable mod', from: 'user'});
			};

			dAmn.sendMsg = function (channel, msg) {
				self.Context.fs.stat(
					'tests/resources/listeners/mod.js'
					, function (err, stat) {
						test.strictEqual(err, null);

						self.Context.fs.stat(
							'tests/resources/listeners/disabled/mod.js'
							, function (err, stat) {
								test.strictEqual(err.code, 'ENOENT');
								test.done();
							}
						);
					}
				);
			};

			new listeners(self.Context, EventProcessor, dAmn);
		};
	}
};

