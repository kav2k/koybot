'use strict';

var greet = require('../listeners/recv_greet.js');
var utils = require('../lib/test_utils.js');

var getMockContext = function (attach) {
	var Context = {};
	var Tablumps = require('../lib/tablumps.js');

	Context.MathsUtils = require('../lib/maths_utils.js');
	Context.fmt = require('util').format;
	Context.Tablumps = new Tablumps(Context);
	Context.config = {quoteChance: 1};
	Context.maindb = {};

	utils.interceptLogging(Context, attach);

	return Context;
};

var getMockDAmn = function () {
	return {
		sendAction: function () {}
	};
};

exports.testPullQuoteError = function (test) {
	test.expect(1);

	var Context = getMockContext(this);
	var EventProcessor = utils.getMockEventProcessor({from: 'user'});
	Context.maindb.all = utils.injectError();

	var gr = new greet(Context, EventProcessor);
	test.strictEqual(
		this.error
		, 'Unable to get list of quotes for user user: bad');

	test.done();
};

exports.testNoRow = function (test) {
	test.expect(1);

	var Context = getMockContext(this);
	var EventProcessor = utils.getMockEventProcessor({from: 'user'});
	var dAmn = getMockDAmn();

	Context.maindb.all = function (query, values, cb) {
		cb(null, []);
	};

	dAmn.sendMsg = function (channel, msg) {
		test.strictEqual(msg, 'Hello, user!');
	};

	var gr = new greet(Context, EventProcessor, dAmn);

	test.done();
};

exports.testSuccess = function (test) {
	test.expect(5);

	var Context = getMockContext(this);
	var EventProcessor =
		utils.getMockEventProcessor({from: 'user', channel: 'chat'});
	var dAmn = getMockDAmn();

	Context.memory = {};
	Context.KeyValueStore = {};

	Context.maindb.all = function (query, values, cb) {
		cb(null, [{quote: 'quote1', id: 1}, {quote: 'quote2', id: 2}]);
	};

	Context.KeyValueStore.put = function (key, value, cb) {
		test.strictEqual(key, 'RecentQuoteID');
		test.ok(value === 1 || value === 2);
	};

	dAmn.sendMsg = function (channel, msg) {
		test.strictEqual(channel, 'chat');
		test.ok(['"quote1"', '"quote2"'].indexOf(msg.split('! ')[1]) > -1);
	};

	var gr = new greet(Context, EventProcessor, dAmn);

	test.ok(Context.memory.recentQuoteID === 1
		|| Context.memory.recentQuoteID === 2);

	test.done();
};

exports.testTablumpsConversion = function (test) {
	test.expect(1);

	var Context = getMockContext(this);
	var EventProcessor =
		utils.getMockEventProcessor({from: 'user', channel: 'chat'});
	var dAmn = getMockDAmn();

	Context.memory = {};
	Context.KeyValueStore = {};

	Context.maindb.all = function (query, values, cb) {
		cb(
			null
			, [{
				quote: 'I am &emote\t:(\t15\t15\t:(\t???\t happy!'
				, id: 1
			}]
		);
	};

	Context.KeyValueStore.put = function () {};

	dAmn.sendMsg = function (channel, msg) {
		test.strictEqual(msg, 'Hello, user! "I am :( happy!"');
	};

	new greet(Context, EventProcessor, dAmn);
	test.done();
};

