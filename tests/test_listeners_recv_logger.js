'use strict';

require('../lib/function.js');
var logger = require('../listeners/recv_logger.js');
var testUtils = require('../lib/test_utils.js');
var dbUtils = require('../lib/db_utils.js');
var fmt = require('util').format;
var moment = require('moment');
var exec = require('child_process').exec;

var getMockEventProcessor = function (channel, subPacket, body) {
	return {
		register: function (evt, fn) {
			fn(channel, subPacket, body);
		}
	};
};

var getMockContext = function (attach, db, day) {
	var Context = testUtils.getMockContext(attach);

	Context.logdb = {run: function () {}};
	Context.DatabaseLoader = {};
	Context.fs = {};

	Context.maindb.get = function (query, values, fn) {
		fn(null, {value: day.valueOf()});
	};

	Context.logdb.get = function (query, fn) {
		fn(null, {timestamp: day.valueOf()});
	};

	Context.fs.stat = function (path, fn) {
		fn({code: 'ENOENT'});
	};

	Context.fs.mkdir = function (path, chmod, fn) {
		fn();
	};

	Context.DatabaseLoader.load = function (file, sql, fn) {
		fn(db);
	};

	Context.logdb.all = function (query, values, fn) {
		fn(null, []);
	};

	return Context;
};

exports.testLoggingWithoutUser = function (test) {
	test.expect(1);

	var EventProcessor = getMockEventProcessor('', {}, '');
	var Context = {};
	Context.moment = moment;
	Context.maindb = {get: function () {}};
	Context.logdb = {
		run: function (query, values, cb) {
			test.strictEqual(values.$user, null);
		}
	};

	new logger(Context, EventProcessor);
	test.done();
};

exports.testLoggingWithUser = function (test) {
	test.expect(1);

	var EventProcessor = getMockEventProcessor('', {args: {from: 'a'}}, '');
	var Context = {};
	Context.moment = moment;
	Context.maindb = {get: function () {}};
	Context.logdb = {
		run: function (query, values, cb) {
			test.strictEqual(values.$user, 'a');
		}
	};

	new logger(Context, EventProcessor);
	test.done();
};

exports.testLoggingFails = function (test) {
	test.expect(1);

	var EventProcessor = getMockEventProcessor('', {}, '');
	var Context = {};
	Context.moment = moment;
	Context.maindb = {get: function () {}};
	Context.logdb = {run: testUtils.injectError()};
	testUtils.interceptLogging(Context, this);

	new logger(Context, EventProcessor);
	test.strictEqual(this.error, 'Error logging packet: bad');

	test.done();
};

exports.testRotateLogsError = function (test) {
	test.expect(3);

	var Context = testUtils.getMockContext(this);
	var EventProcessor = getMockEventProcessor('', {}, '');

	Context.logdb = {run: function () {}};
	Context.fs = {stat: function () {}};
	Context.maindb.get = testUtils.injectError();

	new logger(Context, EventProcessor);
	test.strictEqual(
		this.error
		, 'Unable to check if logs should rotate by key, trying by time: bad');

	Context.maindb.get = function (query, values, fn) {
		fn(null, {value: 'NaN'});
	};

	new logger(Context, EventProcessor);
	test.strictEqual(
		this.error
		, 'Error converting stored timestamp to number, database '
		+ 'may be corrupt: ');

	Context.maindb.get = function (query, values, fn) {
		fn(null, undefined);
	};

	Context.maindb.run = testUtils.injectError();

	new logger(Context, EventProcessor);
	test.strictEqual(this.error, 'Unable to store last log time: bad');

	test.done();
};

exports.testDoRotateError = function (test) {
	test.expect(10);

	var Context = testUtils.getMockContext(this);
	var EventProcessor = getMockEventProcessor('', {}, '');

	Context.logdb = {run: function () {}};
	Context.DatabaseLoader = {};
	Context.fs = {};

	Context.maindb.get = function (query, values, fn) {
		fn(null, {value: '0'});
	};

	Context.logdb.get = testUtils.injectError();

	new logger(Context, EventProcessor);
	test.strictEqual(this.error, 'Error checking earliest log timestamp: bad');

	Context.logdb.get = function (query, fn) {
		fn(null, undefined);
	};

	new logger(Context, EventProcessor);
	test.strictEqual(this.error, 'No rows in log database.');

	var earliest = moment().subtract(3, 'days');
	Context.logdb.get = function (query, fn) {
		fn(null, {timestamp: earliest.valueOf()});
	};

	var statCount = 0;
	Context.fs.stat = function (path, fn) {
		if (statCount === 0) {
			fn();
		} else {
			fn({code: 'ENOENT'});
		}

		test.ok(statCount <= 2);
		statCount++;
	};

	var mkdirCount = 0;
	Context.fs.mkdir = function (path, chmod, fn) {
		if (mkdirCount === 1) {
			test.strictEqual(
				path
				, 'logs/' + moment(earliest).add(1, 'day').format('YYYY/MM'));
		} else if (mkdirCount === 3) {
			test.strictEqual(
				path
				, 'logs/' + moment(earliest).add(2, 'days').format('YYYY/MM'));
		}

		mkdirCount++;
		fn();
	};

	Context.DatabaseLoader.load = function (file, sql, fn) {
		fn(null);
	};

	var unlinkCount = 0;
	Context.fs.unlink = function (file, fn) {
		fn({message: 'bad'});

		unlinkCount++;
		var day = moment(earliest).add(unlinkCount, 'days').startOf('day');
		test.strictEqual(
			file
			, fmt('logs/%s.%d.log.sq3', day.format('YYYY/MM/DD'), day.valueOf()));
	};

	new logger(Context, EventProcessor);
	var lastDay = moment(earliest).add(2, 'days').startOf('day');
	test.strictEqual(
		this.error
		, fmt(
			'Unable to delete logs/%s.%d.log.sq3: bad. Please delete manually.'
			, lastDay.format('YYYY/MM/DD')
			, lastDay.valueOf()));

	test.done();
};

exports.testDoCopyError = function (test) {
	test.expect(6);

	var db = {};
	db.close = function () {};

	var yesterday = moment().subtract(1, 'day').startOf('day');
	var Context = getMockContext(this, db, yesterday);
	var EventProcessor = getMockEventProcessor('', {}, '');

	Context.logdb.all = testUtils.injectError();
	Context.fs.unlink = function (path, fn) {
		test.strictEqual(
			path
			, fmt(
				'logs/%s.%d.log.sq3'
				, yesterday.format('YYYY/MM/DD')
				, yesterday.valueOf()));
	};

	new logger(Context, EventProcessor);
	test.strictEqual(
		this.error
		, 'Error transferring chunk of data to temporary log DB: bad');

	Context.logdb.all = function (query, values, fn) {
		fn(null, [{timestamp: 0, channel: 0, user: 0, subpacket: 0}]);
	};

	db.run = testUtils.injectError();

	new logger(Context, EventProcessor);
	test.strictEqual(
		this.error
		, 'Error transferring chunk of data to temporary log DB: bad');

	Context.logdb.all = function (query, values, fn) {
		fn(null, []);
	};

	db.close = function () {};
	db.on = testUtils.injectError();

	Context.FileUtils = {};
	Context.FileUtils.zip = function (file, fn) {
		test.strictEqual(
			file
			, fmt(
				'logs/%s.%d.log.sq3'
				, yesterday.format('YYYY/MM/DD')
				, yesterday.valueOf()));
	};

	new logger(Context, EventProcessor);
	test.strictEqual(
		this.error
		, fmt(
			'Unable to close temporary log database logs/%s.%d.log.sq3: bad'
			, yesterday.format('YYYY/MM/DD')
			, yesterday.valueOf()));

	test.done();
};

exports.testBzipLogsError = function (test) {
	test.expect(1);

	var db = {close: function () {}, on: function () {}};
	var yesterday = moment().subtract(1, 'day').startOf('day');
	var Context = getMockContext(this, db, yesterday);
	var EventProcessor = getMockEventProcessor('', {}, '');

	Context.FileUtils = {};
	Context.FileUtils.zip = function (file, fn) {
		fn(
			1
			, fmt(
				'logs/%s.%d.log.sq3.bz2'
				, yesterday.format('YYYY/MM/DD')
				, yesterday.valueOf()));
	};

	Context.logdb.all = function (query, values, fn) {
		if (values.$offset != null) {
			fn(null, []);
		}
	};

	new logger(Context, EventProcessor);
	test.strictEqual(
		this.error
		, fmt(
			"Unable to compress rotated log file 'logs/%s.%d.log.sq3.bz2'. "
			+ "Please compress and move manually."
			, yesterday.format('YYYY/MM/DD')
			, yesterday.valueOf()));

	test.done();
};

exports.testTruncateLogDBError = function (test) {
	test.expect(6);

	var db = {close: function () {}, on: function () {}};
	var yesterday = moment().subtract(1, 'day').startOf('day');
	var Context = getMockContext(this, db, yesterday);
	var EventProcessor = getMockEventProcessor('', {}, '');

	Context.config = {rotateLogsPreserve: 666};
	Context.FileUtils = {};
	Context.FileUtils.zip = function (file, fn) {
		fn(
			0
			, fmt(
				'logs/%s.%d.log.sq3.bz2'
				, yesterday.format('YYYY/MM/DD')
				, yesterday.valueOf()));
	};

	Context.logdb.all = function (query, values, fn) {
		if (values.$offset != null) {
			fn(null, []);
		} else {
			test.strictEqual(values.$limit, 666);
			fn({message: 'bad'});
		}
	};

	new logger(Context, EventProcessor);
	test.strictEqual(
		this.error
		, 'Error determining timestamp to preserve '
		+ 'during log rotation: bad');

	Context.logdb.all = function (query, values, fn) {
		fn(null, []);
	};

	new logger(Context, EventProcessor);
	test.strictEqual(
		this.error
		, 'Error determining timestamp to preserve '
		+ 'during log rotation: no rows returned');

	Context.logdb.all = function (query, values, fn) {
		if (values.$offset != null) {
			fn(null, []);
		} else {
			fn(null, [{timestamp: moment().add(1, 'day').valueOf()}]);
		}
	};

	Context.logdb.run = function (query, values, fn) {
		if (values.$channel == null) {
			test.strictEqual(
				values.$timestamp
				, moment().startOf('day').valueOf());

			fn({message: 'bad'});
		}
	};

	new logger(Context, EventProcessor);
	test.strictEqual(
		this.error
		, 'Unable to remove rotated rows from log DB: bad');

	Context.logdb.run = function (query, values, fn) {
		if (values.$channel == null) {
			fn();
		}
	};

	Context.maindb.run = testUtils.injectError();

	new logger(Context, EventProcessor);
	test.strictEqual(
		this.error
		, 'Unable to update last log rotation time: bad');

	test.done();
};

exports.integrationTests = {
	setUp: function (cb) {
		var self = this;
		var DatabaseLoader = require('../lib/database_loader.js');

		self.Context = {};
		self.Context.config = {logDirectory: 'tests/logs'};
		self.Context.fs = require('fs');
		self.Context.sqlite3 = require('sqlite3');
		self.Context.FileUtils = require('../lib/file_utils.js');
		testUtils.interceptLogging(self.Context, self);

		self.Context.moment = moment;
		self.Context.fmt = fmt;
		self.Context.DatabaseLoader = new DatabaseLoader(self.Context);
		self.today = moment().startOf('day');

		self.logpath = function (day) {
			return fmt(
				'tests/logs/%s.%d.log.sq3.bz2'
				, day.format('YYYY/MM/DD')
				, day.valueOf());
		};

		var logData = [];

		for (var i = 0; i < 4; i++) {
			for (var n = 0; n < 50; n++) {
				var timestamp =
					moment(self.today)
					.subtract(i, 'day')
					.valueOf()
					+ n * 32768;

				logData.push([
					timestamp
					, 'chan'
					, 'user'
					, 'msg main\n\nbody\n\0'
				]);
			}
		}

		var data = {
			logdb: [
				{tableName: 'log'
				, cols: ['timestamp', 'channel', 'user', 'subpacket']}
			]
		};

		data.logdb = data.logdb.concat(logData);
		dbUtils.setupDBs(self.Context, cb, data);
	},

	tearDown: function (cb) {
		dbUtils.cleanupDBs(this.Context, function () {
			exec('rm -rf tests/logs/*', function (err, stdout, stderr) {
				if (err != null) {
					throw err;
				}

				cb();
			});
		});
	},

	testFallback: function (test) {
		test.expect(7);

		var self = this;
		var EventProcessor = getMockEventProcessor('', {}, '');

		var run = self.Context.maindb.run;
		self.Context.maindb.run = function (query, values, fn) {
			if (values.$now != null) {
				for (var i = 1; i < 4; i++) {
					test.ok(
						self.Context.fs.statSync(
							self.logpath(
								moment(self.today)
								.subtract(i, 'days')))
						.isFile());
				}

				run.call(self.Context.maindb, query, values, function (err) {
					fn(err);

					test.strictEqual(self.error, undefined);
					test.strictEqual(self.log, undefined);

					self.Context.maindb.get(
						"SELECT value FROM misc WHERE key = 'LastLogRotation'"
						, function (err, row) {
							test.strictEqual(err, null);
							test.strictEqual(
								parseInt(row.value)
								, self.today.valueOf());

							test.done();
						}
					);
				});
			} else {
				run.call(self.Context.maindb, query, values, fn);
			}
		};

		new logger(self.Context, EventProcessor);
	},

	testRotateByKey: function (test) {
		test.expect(10);

		var self = this;
		var EventProcessor = getMockEventProcessor('', {}, '');
		var yesterday = moment(self.today).subtract(1, 'day').valueOf();
		var run = self.Context.maindb.run;
		var get = self.Context.maindb.get;

		self.Context.maindb.run = function (query, values, fn) {
			if (values.$now != null) {
				for (var i = 1; i < 4; i++) {
					test.ok(
						self.Context.fs.statSync(
							self.logpath(
								moment(self.today)
								.subtract(i, 'days')))
						.isFile());
				}

				run.call(self.Context.maindb, query, values, function (err) {
					fn(err);

					test.strictEqual(self.error, undefined);
					test.strictEqual(self.log, undefined);

					get.call(
						self.Context.maindb
						, "SELECT value FROM misc WHERE key = 'LastLogRotation'"
						, function (err, row) {
							test.strictEqual(err, null);
							test.strictEqual(
								parseInt(row.value)
								, self.today.valueOf());

							test.done();
						}
					);
				});
			} else {
				run.call(self.Context.maindb, query, values, fn);
			}
		};

		self.Context.maindb.get = function (query, values, fn) {
			if (values.$key != null) {
				get.call(
					self.Context.maindb
					, query
					, values
					, function (err, row) {
						test.strictEqual(err, null);
						test.deepEqual(row, {value: yesterday});
						fn(err, row);
					}
				);
			} else {
				get.call(self.Context.maindb, query, values, fn);
			}
		};

		self.Context.maindb.run(
			"INSERT INTO misc (key, value) "
			+ "VALUES ('LastLogRotation', $yesterday)"
			, {$yesterday: yesterday}
			, function (err) {
				test.strictEqual(err, null);
				new logger(self.Context, EventProcessor);
			}
		);
	},

	testYesterdaysLogAlreadyExists: function (test) {
		test.expect(1);

		var self = this;
		var EventProcessor = getMockEventProcessor('', {}, '');
		var yesterday = moment(self.today).subtract(1, 'day').startOf('day');
		var logpath = self.logpath(yesterday);

		self.Context.fs.mkdirSync('tests/logs/' + yesterday.format('YYYY'));
		self.Context.fs.mkdirSync('tests/logs/' + yesterday.format('YYYY/MM'));
		self.Context.fs.writeFileSync(logpath, 1);

		self.Context.logdb.get = function (query, values, fn) {
			test.ok(false);
		};

		// Apparently you should never do this.
		var stat = self.Context.fs.stat;
		self.Context.fs.stat = function (path, fn) {
			stat.call(self.Context.fs, path, function (err, stats) {
				fn(err, stats);
				test.strictEqual(err, null);
				self.Context.fs.stat = stat;
			});
		};

		new logger(self.Context, EventProcessor);
		test.done();
	},

	testLastRotatedToday: function (test) {
		test.expect(1);

		var self = this;
		var EventProcessor = getMockEventProcessor('', {}, '');

		self.Context.logdb.get = function (query, values, fn) {
			test.ok(false);
		};

		var get = self.Context.maindb.get;
		self.Context.maindb.get = function (query, values, fn) {
			get.call(self.Context.maindb, query, values, function (err, row) {
				fn(err, row);
				test.done();
			});
		};

		self.Context.maindb.run(
			"INSERT INTO misc (key, value) "
			+ "VALUES ('LastLogRotation', $today)"
			, {$today: self.today.valueOf()}
			, function (err) {
				test.strictEqual(err, null);
				new logger(self.Context, EventProcessor);
			}
		);
	}
};

