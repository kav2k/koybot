'use strict';

var fmt = require('util').format;
var EventEmitter = require('events').EventEmitter;
var OAuth = require('../lib/oauth.js');

var generateContextHttps = function (Context, dataStr) {
	Context.https = {
		get: function (url, fn) {
			var res = new EventEmitter();
			fn.call(res, res);
			res.emit('data', dataStr);
			res.emit('end');

			return new EventEmitter();
		}
	};
};

var generateContextDamntoken = function (Context, dataStr) {
	Context.https = {
		get: function (url, fn) {
			var res = new EventEmitter();
			fn.call(res, res);

			if (url.indexOf('damntoken') < 0) {
				res.emit('data', '{"expires_in": 1000000, "access_token": 1}');
			} else {
				res.emit('data', dataStr);
			}

			res.emit('end');

			return new EventEmitter();
		}
	};

	Context.fs.writeFile = function (file, data, options, fn) {
		fn();
	};
};

exports.tests = {
	setUp: function (cb) {
		var self = this;
		self.Context = {
			Log: {
				error: function () {
					var args = Array.prototype.slice.call(arguments, 0);
					var msg = fmt.apply(self, args);

					if (self.error == null) {
						self.error = msg;
					} else {
						if (self.error.push) {
							self.error.push(msg);
						} else {
							self.error = [self.error, msg];
						}
					}
				}
			},
			config: {
				oAuthStore: 'tests/resources/test_oauth_store_valid.json'
			},
			fmt: fmt,
			fs: require('fs')
		};

		self.error = null;
		cb();
	},

	testInvalidStore: function (test) {
		test.expect(1);

		this.Context.config.oAuthStore = '404';
		var oauth = new OAuth(this.Context);

		oauth.getAccessToken('', '', function () {});
		test.strictEqual(
			this.error
			, 'Cannot refresh oAuth token, no refresh token available.');

		test.done();
	},

	testNoCallbackProvided: function (test) {
		test.expect(1);

		var oauth = new OAuth(this.Context);
		oauth.getAccessToken('', '', '');

		test.strictEqual(
			this.error
			, 'oauth.refresh requires a callback argument.');

		test.done();
	},

	testBadOAuthData: function (test) {
		test.expect(1);

		generateContextHttps(this.Context, 'invalid JSON');

		var oauth = new OAuth(this.Context);
		oauth.getAccessToken('', '', function () {});

		test.strictEqual(
			this.error
			, 'FATAL: Unable to refresh OAuth token: Unexpected token i');

		test.done();
	},

	testCorruptResponse: function (test) {
		test.expect(1);

		generateContextHttps(this.Context, '{}');

		var oauth = new OAuth(this.Context);
		oauth.getAccessToken('', '', function () {});

		test.strictEqual(this.error, 'OAuth token response corrupt: {}');

		test.done();
	},

	testErrorOnPersist: function (test) {
		test.expect(1);

		generateContextHttps(
			this.Context
			, '{"expires_in": 1, "access_token": 1}');

		this.Context.fs.writeFile = function (file, data, options, fn) {
			fn({message: 'bad'});
		};

		var oauth = new OAuth(this.Context);
		oauth.getAccessToken('', '', function () {});

		test.strictEqual(
			this.error[0]
			, 'Error persisting OAuth data, bot will be unable to '
			+ 'authenticate in future: bad'
		);

		test.done();
	},

	testInvalidDAmnTokenResponse: function (test) {
		test.expect(1);

		generateContextDamntoken(this.Context, 'invalid json');

		var oauth = new OAuth(this.Context);
		oauth.getAccessToken('', '', function () {});

		test.strictEqual(
			this.error
			, 'Invalid response when retrieving dAmn token: invalid json');

		test.done();
	},

	testNoDamntokenProperty: function (test) {
		test.expect(1);

		generateContextDamntoken(this.Context, '{}');

		var oauth = new OAuth(this.Context);
		oauth.getAccessToken('', '', function () {});

		test.strictEqual(
			this.error
			, 'Invalid response when retrieving dAmn token: {}');

		test.done();
	},

	testRetrieveDAmnToken: function (test) {
		test.expect(1);

		generateContextDamntoken(this.Context, '{"damntoken": 1}');

		var oauth = new OAuth(this.Context);
		oauth.getAccessToken('', '', function () {});

		test.strictEqual(oauth.dAmnToken, 1);

		test.done();
	},

	testExpiryCache: function (test) {
		// Only 3 because one should never be called as part of the test.
		test.expect(3);

		generateContextDamntoken(this.Context, '{"damntoken": 1}');

		var oauth = new OAuth(this.Context);
		test.equal(oauth.dAmnToken, null);
		oauth.getAccessToken('', '', function () {});
		test.strictEqual(oauth.dAmnToken, 1);

		this.Context.https.get = function () {
			// Should never be called.
			test.ok(false);
		};

		oauth.getAccessToken('', '', function (token) {
			test.strictEqual(token, 1);
		});

		test.done();
	}
};
