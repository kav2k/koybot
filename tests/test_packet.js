'use strict';

var fmt = require('util').format;
var Packet = require('../lib/packet.js');
var utils = require('../lib/test_utils.js');

exports.tests = {
	setUp: function (cb) {
		var self = this;
		var Context = {};
		utils.interceptLogging(Context, self);
		self.error = null;
		self.packet = new Packet(Context);
		cb();
	},

	testNoArgs: function (test) {
		test.expect(1);
		test.equal(this.packet.parse(), null);
		test.done();
	},

	testPacketStartsWithNewline: function (test) {
		test.expect(1);
		var result = this.packet.parse('\n');
		test.strictEqual(result, null);
		test.done();
	},

	testPacketIncorrectArgs: function (test) {
		test.expect(1);
		var result = this.packet.parse('cmd\narg1');
		test.deepEqual(
			result, {cmd: 'cmd', param: undefined, args: null, body: null});
		test.done();
	},

	testSubpacketPreserved: function (test) {
		test.expect(1);
		var result = this.packet.parse('cmd\n\nmsg main\narg1=val\n\nsub-body');
		test.deepEqual(
			result
			, {
				cmd: 'cmd'
				, param: undefined
				, args: {}
				, body: 'msg main\narg1=val\n\nsub-body'
			});
		test.done();
	},

	testPacketCorrect: function (test) {
		test.expect(1);
		var result = this.packet.parse('cmd param\narg1=val\narg2=val\n\nbody');
		test.deepEqual(
			result
			, {
				cmd: 'cmd'
				, param: 'param'
				, args: {arg1: 'val', arg2: 'val'}
				, body: 'body'
			});
		test.done();
	},

	testGetChannelNullParam: function (test) {
		test.expect(1);
		this.packet.getChannel({});
		test.strictEqual(this.error, 'Packet has no channel.');
		test.done();
	},

	testGetChannelNoColon: function (test) {
		test.expect(1);
		this.packet.getChannel({param: 'param'});
		test.strictEqual(this.error, 'Packet has no channel.');
		test.done();
	},

	testGetChannel: function (test) {
		test.expect(1);
		var result = this.packet.getChannel({param: 'chat:chatroom'});
		test.strictEqual(result, 'chatroom');
		test.done();
	},

	testPacketCreateNoCmd: function (test) {
		test.expect(2);
		var result = this.packet.create();
		test.strictEqual(result, '');
		test.strictEqual(
			this.error
			, 'Unable to create packet: Invalid command.');
		test.done();
	},

	testPacketCreate: function (test) {
		test.expect(5);

		var result = this.packet.create('cmd');
		test.strictEqual(result, 'cmd');

		result = this.packet.create('cmd', 'param');
		test.strictEqual(result, 'cmd param');

		result = this.packet.create('cmd', null, null, 'body');
		test.strictEqual(result, 'cmd\n\nbody');

		result = this.packet.create('cmd', null, {arg1: 'val', arg2: 'val'});
		test.strictEqual(result, 'cmd\narg1=val\narg2=val');

		result = this.packet.create(
			'cmd'
			, 'param'
			, {arg1: 'val', arg2: 'val'}
			, 'body');
		test.strictEqual(result, 'cmd param\narg1=val\narg2=val\n\nbody');

		test.done();
	},

	testExtractCmdArgsNoArgs: function (test) {
		test.expect(4);

		var result = this.packet.extractCmdArgs({});
		test.deepEqual(result, []);
		test.strictEqual(
			this.error
			, 'Unable to extract command arguments from: {}');

		result = this.packet.extractCmdArgs({body: '`quote'});
		test.deepEqual(result, []);

		result = this.packet.extractCmdArgs({content: '`quote '});
		test.deepEqual(result, []);

		test.done();
	},

	testExtractCmdArgs: function (test) {
		test.expect(2);

		var result = this.packet.extractCmdArgs('`quote 1 2 3');
		test.deepEqual(result, [1, 2, 3]);

		result = this.packet.extractCmdArgs('`quote  1  2  3');
		test.deepEqual(result, ['', 1, '', 2, '', 3]);

		test.done();
	},

	testExtractCmdArgsQuotes: function (test) {
		test.expect(2);

		var result = this.packet.extractCmdArgs('`quote "1 2" 3');
		test.deepEqual(result, ['1 2', 3]);

		result = this.packet.extractCmdArgs('`quote "123  456 " "789 """" "');
		test.deepEqual(result, ['123  456 ', '789  ']);

		test.done();
	},

	testExtractCmdArgsEscape: function (test) {
		test.expect(3);

		var result = this.packet.extractCmdArgs('`quote \\ 1\\ 2');
		test.deepEqual(result, ['', 1, 2]);

		result = this.packet.extractCmdArgs('`quote \\\\1 \\2');
		test.deepEqual(result, ['\\1', 2]);

		result = this.packet.extractCmdArgs('`quote \\"1 2\\" \\"3" "4');
		test.deepEqual(result, ['"1', '2"', '"3 4']);

		test.done();
	},

	testTrimTrailingNullCharacter: function (test) {
		test.expect(1);

		var result = this.packet.parse(
			'recv chat:chatroom\n\nmsg main\nfrom=user\n\nbody\u0000');
		test.deepEqual(result, {
			cmd: 'recv'
			, param: 'chat:chatroom'
			, args: {}
			, body: 'msg main\nfrom=user\n\nbody'
		});

		test.done();
	},

	testHasArg: function (test) {
		test.expect(3);

		var result = this.packet.hasArg({body: '!command'});
		test.strictEqual(result, false);

		result = this.packet.hasArg({content: '!command '});
		test.strictEqual(result, false);

		result = this.packet.hasArg('!command arg1 arg2');
		test.strictEqual(result, 'arg1 arg2');

		test.done();
	}
};
