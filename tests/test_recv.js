'use strict';

var fmt = require('util').format;
var Recv = require('../lib/recv.js');
var Packet = require('../lib/packet.js');
var utils = require('../lib/test_utils.js');

exports.tests = {
	setUp: function (cb) {
		var self = this;
		self.Context = {cmdLine: {prettyLogs: false}};
		self.Context.Packet = new Packet(self.Context);
		self.Context.config = {cmdChar: ['!'], name: 'bot'};
		utils.interceptLogging(self.Context, self);

		self.recv = new Recv(this.Context, {fire: function () {}});
		self.error = null;
		self.log = null;
		cb();
	},

	testPacketNoBody: function (test) {
		test.expect(1);
		this.recv.handle({});
		test.strictEqual(this.error, 'Received corrupt RECV packet.');
		test.done();
	},

	testPacketNoChannel: function (test) {
		test.expect(1);
		this.recv.handle({body: 'body'});
		test.strictEqual(this.error, 'Received corrupt RECV packet.');
		test.done();
	},

	testPacketCorruptSubPacket: function (test) {
		test.expect(1);
		this.recv.handle({param: 'chat:chatroom', body: '\n'});
		test.strictEqual(this.error, 'Received corrupt RECV packet.');
		test.done();
	},

	testFiresRecv: function (test) {
		test.expect(4);

		var eventProcessor = {
			fire: function (eventName, channel, subPacket, body) {
				test.strictEqual(eventName, 'recv');
				test.strictEqual(channel, 'chatroom');
				test.deepEqual(
					subPacket
					, {cmd: 'cmd', param: undefined, args: {}, body: ''});
				test.strictEqual(body, 'cmd\n');
			}
		};

		var recv = new Recv(this.Context, eventProcessor);
		recv.handle({
			param: 'chat:chatroom'
			, body: 'cmd\n'
		});

		test.done();
	},

	testCorrectRecvObjectFormat: function (test) {
		test.expect(2);

		var eventProcessor = {
			count: 0
			, fire: function (eventName, recv) {
				if (this.count % 2 === 1) {
					test.deepEqual(
						recv
						, {
							channel: 'chatroom'
							, action: 'join'
							, from: 'user'
							, content: 'sub-body'
						}
					);
				}

				this.count++;
			}
		};

		var recv = new Recv(this.Context, eventProcessor);

		recv.handle({
			param: 'chat:chatroom'
			, body: fmt('%s user\narg1=val\n\nsub-body', recv.Actions.JOIN)
		});

		recv.handle({
			param: 'chat:chatroom'
			, body: fmt('%s\nfrom=user\n\nsub-body', recv.Actions.JOIN)
		});

		test.done();
	},

	testFiresRecvJoin: function (test) {
		test.expect(1);

		var eventProcessor = {
			count: 0
			, fire: function (eventName) {
				if (this.count < 1) {
					this.count++;
					return;
				}

				test.strictEqual(eventName, 'recvJoin');
			}
		};

		var recv = new Recv(this.Context, eventProcessor);
		recv.handle({
			body: fmt('%s\n', recv.Actions.JOIN)
			, param: 'chat:chatroom'
		});

		test.done();
	},

	testFiresRecvMsg: function (test) {
		test.expect(1);

		var eventProcessor = {
			count: 0
			, fire: function (eventName) {
				if (this.count < 1) {
					this.count++;
					return;
				}

				test.strictEqual(eventName, 'recvMsg');
			}
		};

		var recv = new Recv(this.Context, eventProcessor);
		recv.handle({
			body: fmt('%s\n', recv.Actions.MSG)
			, param: 'chat:chatroom'
		});

		test.done();
	},

	testNoFireCmdOnMsg: function (test) {
		test.expect(0);

		var eventProcessor = {
			fire: function (eventName) {
				if (eventName.substring(0, 3) === 'cmd') {
					test.ok(false);
				}
			}
		};

		var recv = new Recv(this.Context, eventProcessor);
		recv.handle({
			body: fmt('%s\n', recv.Actions.MSG)
			, param: 'chat:chatroom'
		});

		test.done();
	},

	testFireCmdOnCmdChar: function (test) {
		test.expect(2);

		var eventProcessor = {
			fire: function (eventName) {
				if (eventName.substring(0, 3) === 'cmd') {
					test.strictEqual(eventName, 'cmd_quote');
				}
			}
		};

		var recv = new Recv(this.Context, eventProcessor);
		recv.handle({
			body: fmt('%s main\nfrom=notbot\n\n!quote', recv.Actions.MSG)
			, param: 'chat:chatroom'
		});

		eventProcessor = {
			fire: function (eventName) {
				if (eventName.substring(0, 3) === 'cmd') {
					test.strictEqual(eventName, 'cmd_multi');
				}
			}
		};

		recv = new Recv(this.Context, eventProcessor);
		recv.handle({
			param: 'chat:chatroom'
			, body: fmt(
				'%s main\nfrom=notbot\n\n!multi arg1 arg2'
				, recv.Actions.MSG)
		});

		test.done();
	},

	testNoFireCmdFromSelf: function (test) {
		test.expect(0);

		var eventProcessor = {
			fire: function (eventName) {
				if (eventName.substring(0, 3) === 'cmd') {
					test.ok(false);
				}
			}
		};

		var recv = new Recv(this.Context, eventProcessor);
		recv.handle({
			body: fmt('%s main\nfrom=BOT\n\n!multi arg1 arg2', recv.Actions.MSG)
			, param: 'chat:chatroom'
		});

		test.done();
	}
};
