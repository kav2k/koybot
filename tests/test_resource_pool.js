'use strict';

require('../lib/function.js');
var Pool = require('../lib/resource_pool.js');

exports.testResourcePool = function (test) {
	test.expect(5);

	var Context = {};
	Context.Locks = {};
	Context.Locks.create = function () {
		return {
			release: function () {
				test.ok(true);
			}

			, acquire: function (fn) {
				test.ok(true);
			}
		};
	};

	Pool = new Pool(Context);
	var pool = Pool.create(1, function (index) {
		test.strictEqual(index, 0);
		return '' + index;
	});

	pool.acquire(function (resource)  {
		test.strictEqual(resource, '0');
	});

	pool.acquire(function () {});
	pool.release('1');

	pool.acquire(function (resource)  {
		test.strictEqual(resource, '1');
	});

	test.done();
};
